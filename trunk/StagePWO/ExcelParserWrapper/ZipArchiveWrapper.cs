﻿using ExcelParserCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelParserWrapper
{
	public class ZipArchiveWrapper : ZipArchive, IZipArchive
	{
		public ZipArchiveWrapper(Stream stream) : base(stream) { }

		public new IZipArchiveEntry GetEntry(string path)
		{
			ZipArchiveEntry entry = base.GetEntry(path);
			return new ZipArchiveEntryWrapper(entry);
		}
	}

	public class ZipArchiveEntryWrapper : IZipArchiveEntry
	{
		private ZipArchiveEntry entry;
		public ZipArchiveEntryWrapper(ZipArchiveEntry entry) {
			this.entry = entry;
		}

		public Stream Open()
		{
			return entry.Open();
		}
	}
}
