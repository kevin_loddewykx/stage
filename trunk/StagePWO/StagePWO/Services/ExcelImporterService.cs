﻿using StagePWO.Model;
using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage.AccessCache;
using StagePWO.ViewModels;
using Windows.Storage.Streams;
using ExcelParserCore;
using ExcelParserWrapper;
using System.Globalization;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using StagePWO.Exceptions;

namespace StagePWO.Services
{
	public class ExcelImporterService : IImporterService
	{
		private int speedTab = -1;
		private int torqueTab = -1;
		private int efficiencyTab = -1;
		private IResourceLoader _resourceLoader;

		public ExcelImporterService(IResourceLoader resourceLoader)
		{
			this._resourceLoader = resourceLoader;
		}

		public ExcelImporterService(IResourceLoader resourceLoader, int speedTab, int torqueTab, int efficiencyTab)
		{
			this._resourceLoader = resourceLoader;
			this.speedTab = speedTab;
			this.torqueTab = torqueTab;
			this.efficiencyTab = efficiencyTab;
		}
		public async Task<MeasurementData> Import(bool relative, Engine engine)
		{
			MeasurementData data = new MeasurementData();
			data.Engine = engine;

			Dictionary<int, Dictionary<int, string>> speedDic;
			Dictionary<int, Dictionary<int, string>> torqueDic;
			Dictionary<int, Dictionary<int, string>> efficiencyDic;
			if (speedTab == -1)
			{
				using (IRandomAccessStream speed = await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.SpeedToken)).OpenReadAsync(),
					 torque = await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.TorqueToken)).OpenReadAsync(),
					 efficiency = await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.EfficiencyToken)).OpenReadAsync())
				{

					IZipArchive speedZipArchive = new ZipArchiveWrapper(speed.AsStream());
					IZipArchive torqueZipArchive = new ZipArchiveWrapper(torque.AsStream());
					IZipArchive efficiencyZipArchive = new ZipArchiveWrapper(efficiency.AsStream());
					ExcelParser speedParser = new ExcelParser(speedZipArchive);
					ExcelParser torqueParser = new ExcelParser(torqueZipArchive);
					ExcelParser efficiencyParser = new ExcelParser(efficiencyZipArchive);

					speedDic = speedParser.GetDataSheet(1);
					torqueDic = torqueParser.GetDataSheet(1);
					efficiencyDic = efficiencyParser.GetDataSheet(1);
				}
			}
			else
			{
				using (IRandomAccessStream dataStream = await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.SpeedToken)).OpenReadAsync())
				{
					IZipArchive dataZipArchive = new ZipArchiveWrapper(dataStream.AsStream());
					ExcelParser dataParser = new ExcelParser(dataZipArchive);
					speedDic = dataParser.GetDataSheet(speedTab);
					torqueDic = dataParser.GetDataSheet(torqueTab);
					efficiencyDic = dataParser.GetDataSheet(efficiencyTab);
				}
			}

			if (speedDic.Count != torqueDic.Count || torqueDic.Count != efficiencyDic.Count)
			{
				throw new ImportException(_resourceLoader.GetString("rowError"));
			}
			int rowCount = speedDic.Count;
			data.TripletRows = rowCount;
			int columnCount = -1;
			for (int i = 1; i <= rowCount; ++i)
			{
				Dictionary<int, string> speedRow = speedDic[i];
				Dictionary<int, string> torqueRow = torqueDic[i];
				Dictionary<int, string> efficiencyRow = efficiencyDic[i];

				if (columnCount == -1)
				{
					columnCount = speedRow.Count;
					data.TripletColumns = columnCount;
				}
				if (speedRow.Count != columnCount || torqueRow.Count != columnCount || efficiencyRow.Count != columnCount)
				{
					throw new ImportException(_resourceLoader.GetString("columnError"));
				}
				for (int j = 1; j <= columnCount; ++j)
				{
					double s, t, e;

					if (!(double.TryParse(speedRow[j], out s)))
					{
						s = double.Parse(speedRow[j], CultureInfo.InvariantCulture);
					}
					if (!(double.TryParse(torqueRow[j], out t)))
					{
						t = double.Parse(torqueRow[j], CultureInfo.InvariantCulture);
					}
					if (efficiencyRow[j].StartsWith("n", StringComparison.CurrentCultureIgnoreCase))
					{
						e = 0.0;
					}
					else if (!(double.TryParse(efficiencyRow[j], out e)))
					{
						e = double.Parse(efficiencyRow[j], CultureInfo.InvariantCulture);
					}

					if (relative)
					{
						data.AddTriplet(new EETriplet(s, t, e));
					}
					else
					{
						data.AddTriplet(new EETriplet(s / engine.NominalSpeed, t / engine.NominalTorque, e));
					}
				}
			}
			return data;
		}
	}
}