﻿using DropNetRT;
using DropNetRT.Models;
using InternetDetection;
using StagePWO.Repository;
using System;
using System.Threading.Tasks;
using Windows.Security.Authentication.Web;
using Windows.Storage;

namespace StagePWO.Services
{
	public static class DropboxHelperService
	{
		public static readonly string ASK_ON_START = "ASK_ON_START";

		public static readonly string USER_TOKEN = "USER_TOKEN";
		public static readonly string USER_SECRET = "USER_SECRET";
		public static readonly string APP_TOKEN = "cswrs26y1ayz7og";
		public static readonly string APP_SECRET = "svbetck8prkncz1";

		public static readonly string CALCULATED_NAME = "calculatedPoints.csv";
		public static readonly string SPEED_NAME = "speed.csv";
		public static readonly string TORQUE_NAME = "torque.csv";
		public static readonly string EFFICIENCY_NAME = "efficiency.csv";
		public static readonly string DATA_NAME = "data.xml";
		public static readonly string IMAGE_NAME = "image.png";

		public static readonly string XML_ROOT = "Graph";
		public static readonly string XML_SPEED = "n_nom";
		public static readonly string XML_TORQUE = "m_nom";
		public static readonly string XML_EFFICIENCY = "e_label";
		public static readonly string XML_DATE = "date";
		public static readonly string XML_EXTRA = "extra";
		public static readonly string XML_REMARKS = "remarks";

		private static object syncLock = new object();
		private static bool active = false;
		public static async Task ShowDropboxServiceBroker(IDropboxRepo dropboxRepo)
		{
			lock (syncLock)
			{
				if (active)
				{
					return;
				}
				active = true;
			}
			try
			{
				await dropboxRepo.Reset();
				ApplicationData.Current.LocalSettings.Values[USER_TOKEN] = null;
				ApplicationData.Current.LocalSettings.Values[USER_SECRET] = null;
				if (InternetTools.IsConnectedToInternet())
				{
					DropNetClient client = new DropNetClient(APP_TOKEN, APP_SECRET);
					UserLogin requestToken = await client.GetRequestToken();
					Uri url = new Uri(client.BuildAuthorizeUrl(requestToken));
					WebAuthenticationResult broker = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None, url);
					UserLogin accessToken = await client.GetAccessToken();
					ApplicationData.Current.LocalSettings.Values[USER_TOKEN] = accessToken.Token;
					ApplicationData.Current.LocalSettings.Values[USER_SECRET] = accessToken.Secret;
					await dropboxRepo.InitAsync();
				}
			}
			catch(Exception)
			{
			}
			lock (syncLock)
			{
				active = false;
			}
		}
	}
}