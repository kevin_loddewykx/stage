﻿using Microsoft.Practices.Prism.StoreApps.Interfaces;
using StagePWO.Exceptions;
using StagePWO.Model;
using StagePWO.ViewModels;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage.AccessCache;

namespace StagePWO.Services
{
	public class CSVImporterService : IImporterService
	{
		private static char[] SEPARATOR = new char[] { ';' };

		private IResourceLoader _resourceLoader;

		public CSVImporterService(IResourceLoader resourceLoader)
		{
			this._resourceLoader = resourceLoader;
		}

		public async Task<MeasurementData> Import(bool relative, Engine engine)
		{
			// read three files line by line
			using (StreamReader nReader = new StreamReader(await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.SpeedToken)).OpenStreamForReadAsync()),
								mReader = new StreamReader(await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.TorqueToken)).OpenStreamForReadAsync()),
								eeReader = new StreamReader(await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(NewGraphPageViewModel.EfficiencyToken)).OpenStreamForReadAsync()))
			{
				return await ImportStreams(nReader, mReader, eeReader, engine, relative);
			}
		}

		public async Task<MeasurementData> ImportStreams(StreamReader speed, StreamReader torque, StreamReader efficiency, Engine engine, bool relative)
		{
			MeasurementData data = new MeasurementData();
			data.Engine = engine;

			string nLine;
			string mLine;
			string eeLine;
			int size = -1;
			int rowCounter = 0;
			while ((nLine = await speed.ReadLineAsync()) != null
				& (mLine = await torque.ReadLineAsync()) != null
				& (eeLine = await efficiency.ReadLineAsync()) != null)
			{
				string[] nLineArr = nLine.Split(SEPARATOR);
				string[] mLineArr = mLine.Split(SEPARATOR);
				string[] eeLineArr = eeLine.Split(SEPARATOR);
				if (size == -1)
				{
					size = nLineArr.Count();
					data.TripletColumns = size;
				}
				if (nLineArr.Count() != size || mLineArr.Count() != size || eeLineArr.Count() != size)
				{
					throw new ImportException(_resourceLoader.GetString("columnError"));
				}
				for (int j = 0; j < size; j++)
				{
					// try to parse 25,4 and if this fails try 25.4
					double s, t, e;

					if (!(double.TryParse(nLineArr[j], out s)))
					{
						s = double.Parse(nLineArr[j], CultureInfo.InvariantCulture);
					}
					if (!(double.TryParse(mLineArr[j], out t)))
					{
						t = double.Parse(mLineArr[j], CultureInfo.InvariantCulture);
					}
					if (eeLineArr[j].StartsWith("n"))
					{
						e = 0.0;
					}
					else if (!(double.TryParse(eeLineArr[j], out e)))
					{
						e = double.Parse(eeLineArr[j], CultureInfo.InvariantCulture);
					}

					if (relative)
					{
						data.AddTriplet(new EETriplet(s, t, e));
					}
					else
					{
						data.AddTriplet(new EETriplet(s / engine.NominalSpeed, t / engine.NominalTorque, e));
					}
				}
				++rowCounter;
			}

			if (nLine == null && mLine == null && eeLine == null)
			{
				data.TripletRows = rowCounter;
			}
			else
			{
				throw new ImportException(_resourceLoader.GetString("rowError"));
			}
			return data;
		}
	}
}