﻿using StagePWO.Model;
using System.Threading.Tasks;

namespace StagePWO.Services
{
	public interface IImporterService
	{
		Task<MeasurementData> Import(bool relative, Engine engine);
	}
}