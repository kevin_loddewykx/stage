﻿using Microsoft.Practices.Prism.StoreApps;
using Windows.UI.Xaml;
using StagePWO.ViewModels;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace StagePWO.Views
{
	public sealed partial class MainPage : VisualStateAwarePage, INotifyPropertyChanged
	{
		public int graphViewHeight;
		public int graphViewWidth;

		#region Property Changed handlers
		public event PropertyChangedEventHandler PropertyChanged;

		public bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
		{
			if (object.Equals(storage, value)) return false;

			storage = value;
			this.OnPropertyChanged(propertyName);

			return true;
		}

		public void OnPropertyChanged(string propertyName)
		{
			var eventHandler = this.PropertyChanged;
			if (eventHandler != null)
			{
				eventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		#endregion

		private bool leftActive = true;
		private bool usingLogicalPageNavigation;
		private MainPageViewModel viewModel;
		private int scrollTo = -1;

		public MainPage()
		{
			this.InitializeComponent();
			GoForwardCommand = new DelegateCommand(GoForward);
			viewModel = (MainPageViewModel)this.DataContext;
		}

		public ICommand GoForwardCommand { get; private set; }

		private void CheckVisibilityChange()
		{
			if (usingLogicalPageNavigation && leftActive && viewModel.GraphCount > 2)
			{
				this.forwardButton.Visibility = Visibility.Visible;
			}
			else
			{
				this.forwardButton.Visibility = Visibility.Collapsed;
			}
			GoBackCommand.RaiseCanExecuteChanged();
		}

		public override bool CanGoBack()
		{
			if (!leftActive)
			{
				return true;
			}
			else
			{
				return base.CanGoBack();
			}
		}


		private void GoForward()
		{
			if (usingLogicalPageNavigation)
			{
				leftActive = false;
				graphGridView.SelectedItems.Remove(viewModel.Graphs[0]);
				graphGridView.SelectedItems.Remove(viewModel.Graphs[1]);	
				graphGridView.ScrollIntoView(viewModel.Graphs[2]);
				CheckVisibilityChange();
			}
		}

		protected override void GoBack(object sender, RoutedEventArgs eventArgs)
		{
			if (usingLogicalPageNavigation)
			{
				leftActive = true;
				if (viewModel.GraphCount == 4)
				{
					graphGridView.SelectedItems.Remove(viewModel.Graphs[3]);
				}
				if (viewModel.GraphCount >= 3)
				{
					graphGridView.SelectedItems.Remove(viewModel.Graphs[2]);
				}
				graphGridView.ScrollIntoView(viewModel.Graphs[0]);
				CheckVisibilityChange();
			}
			else
			{
				this.GoBack(sender, eventArgs);
			}
		}

		private void SetStylesListView()
		{
			GridViewHeight = (int)((!usingLogicalPageNavigation && viewModel.GraphCount > 2) || (usingLogicalPageNavigation && viewModel.GraphCount > 1) ? graphGridView.ActualHeight / 2 : graphGridView.ActualHeight / 1);
			GridViewWidth = (int)(((!usingLogicalPageNavigation && viewModel.GraphCount > 1) ? graphGridView.ActualWidth / 2 : graphGridView.ActualWidth / 1));
		}

		public int GridViewHeight
		{
			get { return graphViewHeight; }
			set
			{
				SetProperty(ref graphViewHeight, value);
			}
		}

		public int GridViewWidth
		{
			get { return graphViewWidth; }
			set
			{
				SetProperty(ref graphViewWidth, value);
			}
		}

		private void GraphListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			BottomAppBar.IsOpen = BottomAppBar.IsSticky = graphGridView.SelectedItems.Count > 0;
		}

		private void ViewStates_CurrentStateChanging(object sender, VisualStateChangedEventArgs e)
		{
			// If you only mention PortraitLayout inside the XAML, and the app gets booted in the MinimalLayout this method will not be called.
			if (e.NewState.Name.Equals(PortraitLayoutVisualState) || e.NewState.Name.Equals(MinimalLayoutVisualState))
			{
				if (e.OldState != null && e.OldState.Name.Equals(DefaultLayoutVisualState))
				{
					if (viewModel.GraphCount == 4)
					{
						graphGridView.SelectedItems.Remove(viewModel.Graphs[3]);
					}
					if (viewModel.GraphCount >= 3)
					{
						graphGridView.SelectedItems.Remove(viewModel.Graphs[2]);
					}
				}

				usingLogicalPageNavigation = true;
			}
			else
			{
				leftActive = true;
				usingLogicalPageNavigation = false;
			}
			SetStylesListView();
			CheckVisibilityChange();
		}

		private void GraphListView_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			SetStylesListView();
			if (leftActive)
			{
				scrollTo = 0;
			}
			else
			{
				scrollTo = 2;
			}
		}

		private void addButton_Click(object sender, RoutedEventArgs e)
		{
			if (viewModel.AddGraph())
			{
				SetStylesListView();

				scrollTo = -1;
				if (usingLogicalPageNavigation && viewModel.GraphCount > 2)
				{
					leftActive = false;
					graphGridView.SelectedItems.Remove(viewModel.Graphs[0]);
					graphGridView.SelectedItems.Remove(viewModel.Graphs[1]);
					scrollTo = 2;
				}
				CheckVisibilityChange();
			}
		}

		private void removeButton_Click(object sender, RoutedEventArgs e)
		{
			if (viewModel.RemoveGraphs())
			{
				SetStylesListView();
				if (usingLogicalPageNavigation && viewModel.GraphCount <= 2)
				{
					leftActive = true;
				}
				CheckVisibilityChange();
			}
		}

		// Needed for when a graph is added.
		private void graphGridView_LayoutUpdated(object sender, object e)
		{
			if (scrollTo != -1)
			{
				graphGridView.ScrollIntoView(viewModel.Graphs[scrollTo]);
				scrollTo = -1;
			}
		}
	}
}