﻿using Microsoft.Practices.Prism.StoreApps;
using Windows.UI.Xaml.Controls;

namespace StagePWO.Views
{
	public sealed partial class DropboxPage : VisualStateAwarePage
	{
		public DropboxPage()
		{
			this.InitializeComponent();
		}

		private void CommandBar_Opened(object sender, object e)
		{
			if (this.dropboxGridView.SelectedItems.Count == 0)
			{
				this.BottomAppBar.IsOpen = false;
			}
		}

		private void dropboxGridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.dropboxGridView == null)
			{
				return;
			}
			if (this.dropboxGridView.SelectedItems.Count == 0)
			{
				this.BottomAppBar.IsOpen = false;
			}
			else
			{
				this.BottomAppBar.IsOpen = true;
			}
		}
	}
}