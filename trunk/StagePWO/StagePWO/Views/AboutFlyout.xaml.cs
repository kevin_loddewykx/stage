﻿using System;
using Windows.ApplicationModel;
using Windows.UI.Xaml.Controls;

namespace StagePWO.Views
{
	public sealed partial class AboutFlyout : SettingsFlyout
	{
		public AboutFlyout()
		{
			this.InitializeComponent();
			this.DataContext = this;
		}

		public String VersionString
		{
			get
			{
				PackageVersion version = Package.Current.Id.Version;
				return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
			}
		}
	}
}