﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml.Data;

namespace StagePWO.Converters
{
	public class NullToBooleanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			if (value == null)
			{
				return false;
			}
			IEnumerable<object> list = value as IEnumerable<object>;
			if (list == null || list.Count() > 0)
			{
				return true;
			}
			return false;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}