﻿using System;
using Windows.UI.Xaml.Data;
using StagePWO.Common;
using Windows.ApplicationModel.Resources;

namespace StagePWO.Converters
{
	public class DateTimeToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			if (value == null || !(value is DateTime))
			{
				return null;
			}
			DateTime date = (DateTime)value;

			return date.ToString("yyyy-MMM") + " " + ResourceLoader.GetForCurrentView().GetString("groupingWeek") + ": " + date.WeekOfMonth();
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}