﻿using DropNetRT;
using DropNetRT.Exceptions;
using DropNetRT.Models;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using StagePWO.Model;
using StagePWO.Common;
using StagePWO.Services;
using StagePWO.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.Storage;
using Windows.UI.Core;
using Microsoft.Practices.Prism.StoreApps;
using InternetDetection;
using System.Threading;
using System.Net;
using System.Runtime.Serialization;

namespace StagePWO.Repository
{
	public class DropboxRepo : BindableBase, IDropboxRepo
	{
		private readonly TimeSpan REFRESH_INTERVAL = new TimeSpan(0, 5, 0);

		private readonly string DROPBOXDATA = "PXL_Energy_DropboxData";

		private ICacheService _cacheService;
		private IResourceLoader _resourceLoader;
		private CoreDispatcher dispatcher;
		private ObservableCollection<DropboxGroupViewModel> dbDataVisible;

		private object addDelVisibleLock = new object();
		private object dataCacheLock = new object();
		private bool loadingData;
		private CancellationTokenSource tokenSource;
		private string errorString;

		public DropboxRepo(ICacheService cacheService, IResourceLoader resourceLoader)
		{
			this._cacheService = cacheService;
			this._resourceLoader = resourceLoader;
			DeletedIds = new List<string>();
			AddedIds = new List<string>();
			dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
			InternetTools.InternetConnectionChanged += async (x) => await InternetChangedAsync(x);
			new DelegateCommand(async () => await InternetChangedAsync(InternetTools.IsConnectedToInternet())).Execute();
		}

		public async Task Reset()
		{
			if (tokenSource != null)
			{
				tokenSource.Cancel();
				tokenSource = null;
			}
			LoadingData = false;
			DBDataCache = null;
			DBDataVisible = null;
			DeletedIds = new List<string>();
			AddedIds = new List<string>();
			await _cacheService.SaveDataAsync<ObservableCollection<DropboxGroupViewModel>>(DROPBOXDATA, null);
		}

		public async Task InitAsync()
		{
			await Reset();
			await InternetChangedAsync(InternetTools.IsConnectedToInternet());
		}

		public string ErrorString { get { return errorString; } set { SetProperty(ref errorString, value); } }

		private async Task InternetChangedAsync(bool connected)
		{
			try
			{
				if (ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_SECRET))
				{
					await InitDBDataFromCacheAsync();
					if (connected)
					{
						if (tokenSource == null)
						{
							await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
							{
								if (DBDataVisible.Count > 0)
								{
									LoadingData = false;
								}
								else
								{
									LoadingData = true;
								}
								ErrorString = null;
							});
							tokenSource = new CancellationTokenSource();
							LoadingCycle(tokenSource.Token);
						}
					}
					else
					{
						if (tokenSource != null)
						{
							// Safety for when switching adapters,  Ethernet -> Wifi
							new System.Threading.ManualResetEvent(false).WaitOne(5);
							if (InternetTools.IsConnectedToInternet())
							{
								return;
							}
							await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { ErrorString = null; });
							tokenSource.Cancel();
							tokenSource = null;
						}
						if (DBDataCache.Count == 0)
						{
							await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
							{
								LoadingData = false;
								ErrorString = _resourceLoader.GetString("dropboxErrorInternet");
							});
						}
					}
				}
				else
				{
					await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
					{
						LoadingData = false;
						ErrorString = _resourceLoader.GetString("dropboxErrorAccount");
					});
				}
			}
			catch (Exception)
			{
			}
		}

		private void LoadingCycle(CancellationToken token)
		{
			// Get of the UI thread
			Task.Factory.StartNew(async () =>
				{
					bool error = false;
					try
					{
						while (!token.IsCancellationRequested)
						{
							token.ThrowIfCancellationRequested();
							await LoadDropboxDataAsync(token);
							token.ThrowIfCancellationRequested();
							new System.Threading.ManualResetEvent(false).WaitOne(REFRESH_INTERVAL);
						}
					} catch(OperationCanceledException e) {
						throw e;
					} catch (Exception)
					{
						error = true;
					}
					if (error)
					{
						await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
						{
							await Reset();
							ErrorString = _resourceLoader.GetString("dropboxErrorUnexpected");
						});
						token.ThrowIfCancellationRequested();
					}
				}, token);
		}

		private List<string> AddedIds { get; set; }

		private List<string> DeletedIds { get; set; }

		public async Task AddAdded(DropboxDataViewModel data, DateTime date)
		{
			AddedIds.Add(data.FolderId);

			if (DBDataVisible.Count(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.WeekOfMonth() == date.WeekOfMonth()) == 0)
			{
				int counter = 0;
				bool inserted = false;
				foreach (DropboxGroupViewModel group in DBDataVisible)
				{
					if (group.Date.Year < date.Year || group.Date.Month < date.Month || group.Date.WeekOfMonth() < date.WeekOfMonth())
					{
						inserted = true;
						break;
					}
					++counter;
				}
				if (!inserted)
				{
					DBDataVisible.Add(new DropboxGroupViewModel(date));
				}
				else
				{
					DBDataVisible.Insert(counter, new DropboxGroupViewModel(date));
				}
			}
			DropboxGroupViewModel dataGroup = DBDataVisible.First(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.WeekOfMonth() == date.WeekOfMonth());
			dataGroup.GraphList.Insert(0, data);
			await SaveData();
		}

		public async Task SaveData()
		{
			await _cacheService.SaveDataAsync<ObservableCollection<DropboxGroupViewModel>>(DROPBOXDATA, DBDataVisible);
		}

		public void AddDeleted(IEnumerable<string> folderIds)
		{
			DeletedIds.AddRange(folderIds);

			foreach (DropboxGroupViewModel group in DBDataVisible)
			{
				group.GraphList.RemoveAll((x) => folderIds.Contains(x.FolderId));
			}
			DBDataVisible.RemoveAll(x => x.GraphList.Count == 0);
			AddedIds.RemoveAll(x => folderIds.Contains(x));
			if (DBDataVisible.Count == 0)
			{
				ErrorString = _resourceLoader.GetString("dropboxErrorNoData");
			}
			else
			{
				ErrorString = null;
			}
		}

		public bool LoadingData { get { return loadingData; } set { SetProperty(ref loadingData, value); } }

		public ObservableCollection<DropboxGroupViewModel> DBDataVisible { get { return dbDataVisible; } private set { SetProperty(ref dbDataVisible, value); } }

		private ObservableCollection<DropboxGroupViewModel> DBDataCache { get; set; }

		private async Task InitDBDataFromCacheAsync()
		{
			if (DBDataCache == null)
			{
				try
				{
					DBDataCache = await _cacheService.GetDataAsync<ObservableCollection<DropboxGroupViewModel>>(DROPBOXDATA);
				}
				catch (Exception)
				{
				}
				if (DBDataCache == null)
				{
					DBDataCache = new ObservableCollection<DropboxGroupViewModel>();
				}
				await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
				{
					DBDataVisible = new ObservableCollection<DropboxGroupViewModel>();
					foreach (DropboxGroupViewModel group in DBDataCache)
					{
						DBDataVisible.Add(new DropboxGroupViewModel(group.Date));
						foreach (DropboxDataViewModel data in group.GraphList)
						{
							DropboxDataViewModel dbData = new DropboxDataViewModel
							{
								Data = data.Data,
								FolderId = data.FolderId,
								Date = data.Date,
								Hash = data.Hash,
								ImageData = data.ImageData,
								Title = data.Title
							};
							dbData.InitImage();
							DBDataVisible.Last().GraphList.Add(dbData);
						}
					}
				});
			}
		}

		private async Task LoadDropboxDataAsync(CancellationToken token)
		{
			DropNetClient client = new DropNetClient(DropboxHelperService.APP_TOKEN, DropboxHelperService.APP_SECRET, (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_TOKEN], (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_SECRET]);
			client.UseSandbox = true;
			List<Metadata> metadata = null;
			bool authenticationError = false;
			try
			{
				metadata = (await client.GetMetaData(".")).Contents.Where(md => md.IsDirectory).ToList();
			}
			catch (DropboxException e)
			{
				if (e.Response.StatusCode == HttpStatusCode.Unauthorized || e.Response.StatusCode == HttpStatusCode.Forbidden)
				{
					authenticationError = true;
				}
				else
				{
					throw e;
				}
			}
			await AuthenticationError(token, authenticationError);

			// Remove remote deleted files from local cache
			string[] remoteGuids = metadata.Select((z) => z.Name).ToArray();
			foreach (DropboxGroupViewModel group in DBDataCache)
			{
				group.GraphList.RemoveAll(md => !remoteGuids.Contains(md.FolderId));
			}
			remoteGuids = null;

			token.ThrowIfCancellationRequested();

			// Find all updated files 
			Dictionary<Metadata, DropboxDataViewModel> foldersWithUpdate = new Dictionary<Metadata, DropboxDataViewModel>();
			foreach (DropboxGroupViewModel group in DBDataCache)
			{
				foreach (DropboxDataViewModel data in group.GraphList)
				{
					Metadata folder = metadata.FirstOrDefault(md => md.Name.Equals(data.FolderId));
					
					/* should not happen, safety check for when cache is corrupted */
					if (folder == null)
					{
						continue;
					}

					try
					{
						foldersWithUpdate.Add(await client.GetMetaData(folder.Name, data.Hash), data);
					}
					catch (DropboxException e)
					{
						if (e.Response.StatusCode == HttpStatusCode.Unauthorized || e.Response.StatusCode == HttpStatusCode.Forbidden)
						{
							authenticationError = true;
						}
						// DropboxException will be thrown when hash in cache is the same as the hash on dropbox.
						// This means that the content in the folder hasn't changed.
					}
					await AuthenticationError(token, authenticationError);

					metadata.Remove(folder);
				}
			}

			// Append all new files which are not yet in the cache
			foreach (Metadata folderMeta in metadata)
			{
				try
				{
					foldersWithUpdate.Add(await client.GetMetaData(folderMeta.Name), null);
				}
				catch (DropboxException e)
				{
					if (e.Response.StatusCode == HttpStatusCode.Unauthorized || e.Response.StatusCode == HttpStatusCode.Forbidden)
					{
						authenticationError = true;
					}
					// File got deleted from other thread
				}
				await AuthenticationError(token, authenticationError);
			}
			metadata = null;

			token.ThrowIfCancellationRequested();
			// Update and add new files, allow a maximum of 10 parallel threads.
			await foldersWithUpdate.ForEachAsync(10, async (x) => await AddOrUpdateData(x, client, token), token);

			// Remove all groups which no longer exists, because all their data got deleted
			DBDataCache.RemoveAll(x => x.GraphList.Count == 0);

			// Order the data
			List<DropboxGroupViewModel> orderedCollection = DBDataCache.OrderByDescending(x => x.Date.Year).ThenByDescending(x => x.Date.Month).ThenByDescending(x => x.Date.WeekOfMonth()).ToList();

			token.ThrowIfCancellationRequested();
			// Update visbible data, 
			await UpdateVisibleDataAsync(orderedCollection.DeepClone(), token);
		}

		private async Task AuthenticationError(CancellationToken token, bool error)
		{
			if (error)
			{
				await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
				{
					await Reset();
					ErrorString = _resourceLoader.GetString("dropboxErrorAuthenticate");
					LoadingData = false;
				});
				token.ThrowIfCancellationRequested();
			}
		}

		private async Task UpdateVisibleDataAsync(List<DropboxGroupViewModel> orderedCollection, CancellationToken token)
		{
			IEnumerable<string> foundGuids = orderedCollection.SelectMany((y) => y.GraphList.Select((z) => z.FolderId));
			// All data needs to be disconnected from the cache, so cache can make changes to it in the background
			// without needing to be on the UI Thread. The object DBDataVisibile gets locked to the UI Thread by the binding
			// from the page, and will throw a marshal exception when accessed from another thread.
			await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
			{
				// The data has been remotely deleted and has been detected, no need anymore to store it locally
				DeletedIds.RemoveAll(x => !foundGuids.Contains(x));

				int counter = 0;
				foreach (DropboxGroupViewModel group in orderedCollection)
				{
					if (counter < DBDataVisible.Count)
					{
						ObservableCollection<DropboxDataViewModel> graphList = DBDataVisible[counter].GraphList;
						DateTime date = DBDataVisible[counter].Date;
						bool datesAreEqual = group.Date.Year == date.Year && group.Date.Month == date.Month && group.Date.WeekOfMonth() == date.WeekOfMonth();
						List<string> cacheGuids = null;
						if (datesAreEqual)
						{
							cacheGuids = group.GraphList.Select(z => z.FolderId).ToList();
						}
						else
						{
							cacheGuids = new List<string>();
						}
						// Remove all data which no longer exists
						AddedIds.RemoveAll(x => cacheGuids.Contains(x)); // The data has been remotely added and has been detected, no need anymore to store it locally
						cacheGuids.AddRange(AddedIds);
						cacheGuids.RemoveAll(x => DeletedIds.Contains(x));
						graphList.RemoveAll((x) => !cacheGuids.Contains(x.FolderId));

						if (!datesAreEqual)
						{
							// New group with data which is not yet visible
							group.GraphList = new ObservableCollection<DropboxDataViewModel>(group.GraphList.OrderByDescending(x => x.Date));
							DBDataVisible.Insert(counter, group);
						}
						else
						{
							IEnumerable<string> guids = DBDataVisible[counter].GraphList.Select(z => z.FolderId);

							foreach (DropboxDataViewModel dbData in group.GraphList)
							{
								if (!guids.Contains(dbData.FolderId))
								{
									// New data which is not yet visible, but is in an existing group
									DropboxDataViewModel temp = graphList.FirstOrDefault(x => dbData.Date >= x.Date);
									if (temp != null)
									{
										graphList.Insert(graphList.IndexOf(temp), dbData);
									}
									else
									{
										graphList.Add(dbData);
									}
								}
								else
								{
									// Data already visible but has changed, but is in an existing group
									DropboxDataViewModel data = graphList.First(x => x.FolderId == dbData.FolderId);
									if (data.Hash != dbData.Hash)
									{
										// Check if date has changed
										if (!data.Date.Equals(dbData.Date))
										{
											DropboxDataViewModel temp = graphList.FirstOrDefault(x => dbData.Date >= x.Date);
											if (temp != null)
											{
												graphList.Move(graphList.IndexOf(data), graphList.IndexOf(temp));
											}
											else
											{
												graphList.Move(graphList.IndexOf(data), graphList.Count - 1);
											}
										}
										data.Data = dbData.Data;
										data.FolderId = dbData.FolderId;
										data.Hash = dbData.Hash;
										data.Date = dbData.Date;
										data.ImageData = dbData.ImageData;
										data.Title = dbData.Title;
										data.Image = dbData.Image;
									}
								}
							}
						}
					}
					else
					{
						// New group with new data, and group date is older then current oldest visible group
						group.GraphList = new ObservableCollection<DropboxDataViewModel>(group.GraphList.OrderByDescending(x => x.Date));
						DBDataVisible.Add(group);
					}
					counter++;
				}
				// Remove all groups which no longer exists, because all their data got deleted
				DBDataVisible.RemoveAll(x => x.GraphList.Count == 0);

				foreach (DropboxGroupViewModel group in DBDataVisible)
				{
					foreach (DropboxDataViewModel data in group.GraphList)
					{
						data.InitImage();
					}
				}

				token.ThrowIfCancellationRequested();

				LoadingData = false;
				if (DBDataVisible.Count == 0)
				{
					ErrorString = _resourceLoader.GetString("dropboxErrorNoData");
				}
				else
				{
					ErrorString = null;
				}

				token.ThrowIfCancellationRequested();

				await SaveData();
			});
		}

		private async Task AddOrUpdateData(KeyValuePair<Metadata, DropboxDataViewModel> folder, DropNetClient client, CancellationToken token)
		{
			bool authError = false;
			try
			{
				token.ThrowIfCancellationRequested();

				Metadata fileMeta = folder.Key;
				DropboxDataViewModel dropboxData = folder.Value;
				if (dropboxData == null)
				{
					dropboxData = new DropboxDataViewModel();
				}

				DateTime date = new DateTime();

				MeasurementData data = null;
				byte[] dataFile = null;
				string start = "";
				bool skip = false;
				bool authenticationError = false;
				try
				{
					Engine engine = new Engine();

					dataFile = await client.GetFile("/" + fileMeta.Name + "/" + DropboxHelperService.DATA_NAME);
					start = Encoding.UTF8.GetString(dataFile, 0, 20);

					if (start.IndexOf("error", StringComparison.OrdinalIgnoreCase) == -1)
					{
						XmlReaderSettings xrs = new XmlReaderSettings();
						xrs.Async = true;

						using (XmlReader reader = XmlReader.Create(new MemoryStream(dataFile), xrs))
						{
							while (reader.Read())
							{
								if (reader.NodeType == XmlNodeType.Element)
								{
									if (reader.Name == DropboxHelperService.XML_DATE)
									{
										reader.Read();
										date = DateTime.Parse(reader.Value);
									}
									if (reader.Name == DropboxHelperService.XML_SPEED)
									{
										reader.Read();
										engine.NominalSpeed = Convert.ToInt32(reader.Value);
									}
									if (reader.Name == DropboxHelperService.XML_TORQUE)
									{
										reader.Read();
										engine.NominalTorque = Convert.ToDouble(reader.Value);
									}
									if (reader.Name == DropboxHelperService.XML_EFFICIENCY)
									{
										reader.Read();
										engine.EELabel = reader.Value;
									}
									if (reader.Name == DropboxHelperService.XML_EXTRA)
									{
										reader.Read();
										engine.ExtraInfo = reader.Value;
									}
								}
							}
						}
						dataFile = null;
						dropboxData.FolderId = fileMeta.Name;
						dropboxData.Date = date;
						dropboxData.Hash = fileMeta.Hash;
						dropboxData.Title = "(" + date.ToString("dd/MM/yyyy") + ") " + _resourceLoader.GetString("speedTitle") + engine.NominalSpeed + " " + _resourceLoader.GetString("torqueTitle") + engine.NominalTorque + " " + _resourceLoader.GetString("eeLabelTitle") + engine.EELabel;
						data = await ImportOrginalPoints(client, fileMeta, engine);
						dropboxData.Data = data;
					}
					else
					{
						skip = true;
					}
				}
				catch (DropboxException e)
				{
					if (e.Response.StatusCode == HttpStatusCode.Unauthorized || e.Response.StatusCode == HttpStatusCode.Forbidden)
					{
						authenticationError = true;
					} else {
						skip = true;
					}
				}
				catch (Exception)
				{
					skip = true;
				}
				await AuthenticationError(token, authenticationError);
				if (skip)
				{
					// Remove data from cache
					if (folder.Value != null)
					{
						foreach (DropboxGroupViewModel group in DBDataCache)
						{
							if (group.GraphList.Remove(dropboxData))
							{
								break;
							}
						}
					}
					return;
				}
				token.ThrowIfCancellationRequested();

				lock (dataCacheLock)
				{
					// If new data add it to the cache
					if (folder.Value == null)
					{
						if (DBDataCache.Count(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.WeekOfMonth() == date.WeekOfMonth()) == 0)
						{
							DBDataCache.Add(new DropboxGroupViewModel(date));
						}
						DropboxGroupViewModel group = DBDataCache.First(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.WeekOfMonth() == date.WeekOfMonth());
						group.GraphList.Add(dropboxData);
					}
					// Existing data
					else
					{
						DropboxGroupViewModel group = DBDataCache.First(x => x.GraphList.Contains(dropboxData));
						// Group has changed of the data
						if (group.Date.Year != date.Year || group.Date.Month != date.Month || group.Date.WeekOfMonth() != date.WeekOfMonth())
						{
							var a = group.GraphList.Remove(dropboxData);
							if (DBDataCache.Count(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.WeekOfMonth() == date.WeekOfMonth()) == 0)
							{
								DBDataCache.Add(new DropboxGroupViewModel(date));
							}
							DropboxGroupViewModel group2 = DBDataCache.First(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.WeekOfMonth() == date.WeekOfMonth());
							group2.GraphList.Add(dropboxData);
						}
					}
				}
				token.ThrowIfCancellationRequested();

				// Reset data, for when being updated
				data.XValues = null;
				data.YValues = null;
				data.ValuesFlat = null;

				byte[] file = await client.GetFile(fileMeta.Name + "/" + DropboxHelperService.CALCULATED_NAME);
				token.ThrowIfCancellationRequested();

				start = Encoding.UTF8.GetString(file, 0, 20);
				// check if file is deleted or not found
				if (start.IndexOf("error", StringComparison.OrdinalIgnoreCase) == -1)
				{
					using (StreamReader reader = new StreamReader(new MemoryStream(file)))
					{
						int counter = -1;
						double[] valuesFlat = new double[data.TripletRows * data.TripletColumns * 4];
						string line = null;
						while ((line = await reader.ReadLineAsync()) != null)
						{
							valuesFlat[++counter] = Convert.ToDouble(line);
						}
						if (++counter != valuesFlat.Length)
						{
							throw new Exception();
						}
						double[] xValues = new double[data.TripletColumns * 2];
						double[] yValues = new double[data.TripletRows * 2];
						for (int i = 0; i < data.TripletRows * 2; ++i)
						{
							yValues[i] = 1.5 / (data.TripletRows * 2) * i;
						}
						for (int j = 0; j < data.TripletColumns * 2; ++j)
						{
							xValues[j] = 2.0 / (data.TripletColumns * 2) * j;
						}
						data.XValues = xValues;
						data.YValues = yValues;
						data.ValuesFlat = valuesFlat;
					}

					token.ThrowIfCancellationRequested();

					await ImportImage(client, fileMeta, dropboxData, dispatcher);
				}
			}
			catch (DropboxException e)
			{
				if (e.Response.StatusCode == HttpStatusCode.Unauthorized || e.Response.StatusCode == HttpStatusCode.Forbidden)
				{
					authError = true;
				}
			}
			catch (Exception)
			{
			}
			await AuthenticationError(token, authError);
		}

		private async Task<MeasurementData> ImportOrginalPoints(DropNetClient client, Metadata fileMeta, Engine engine)
		{
			byte[] speedFile = await client.GetFile("/" + fileMeta.Name + "/" + DropboxHelperService.SPEED_NAME);
			byte[] torqueFile = await client.GetFile("/" + fileMeta.Name + "/" + DropboxHelperService.TORQUE_NAME);
			byte[] efficiencyFile = await client.GetFile("/" + fileMeta.Name + "/" + DropboxHelperService.EFFICIENCY_NAME);
			using (StreamReader speed = new StreamReader(new MemoryStream(speedFile)),
				torque = new StreamReader(new MemoryStream(torqueFile)),
				efficiency = new StreamReader(new MemoryStream(efficiencyFile)))
			{
				return await new CSVImporterService(_resourceLoader).ImportStreams(speed, torque, efficiency, engine, true);
			}
		}

		private async Task ImportImage(DropNetClient client, Metadata fileMeta, DropboxDataViewModel dropboxData, CoreDispatcher dispatcher)
		{
			byte[] file = await client.GetFile(fileMeta.Name + "/" + DropboxHelperService.IMAGE_NAME);
			if (file.Length > 20)
			{
				string start = Encoding.UTF8.GetString(file, 0, 20);
				// check if image is deleted or not found
				if (start.IndexOf("error", StringComparison.OrdinalIgnoreCase) == -1)
				{
					dropboxData.ImageData = file;
				}
			}
		}
	}
}