﻿using StagePWO.ViewModels;
using System.Collections.ObjectModel;

namespace StagePWO.Repository
{
	public interface IGraphsRepo
	{
		ObservableCollection<DataViewModel> Graphs { get; }
	}
}