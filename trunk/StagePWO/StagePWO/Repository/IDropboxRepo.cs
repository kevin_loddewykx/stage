﻿using StagePWO.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace StagePWO.Repository
{
	public interface IDropboxRepo
	{
		ObservableCollection<DropboxGroupViewModel> DBDataVisible { get; }

		void AddDeleted(IEnumerable<string> folderIds);

		Task AddAdded(DropboxDataViewModel model, DateTime date);

		Task SaveData();

		bool LoadingData { get; }

		Task InitAsync();

		Task Reset();
	}
}