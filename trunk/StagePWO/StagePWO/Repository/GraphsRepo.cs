﻿using Microsoft.Practices.Prism.StoreApps.Interfaces;
using StagePWO.ViewModels;
using System.Collections.ObjectModel;

namespace StagePWO.Repository
{
	public class GraphsRepo : IGraphsRepo
	{
		private const string sessionStateKey = "GraphsRepo"; 
		public GraphsRepo(ISessionStateService sessionStateService)
		{
			if (sessionStateService.SessionState.ContainsKey(sessionStateKey))
			{
				Graphs = (ObservableCollection<DataViewModel>)sessionStateService.SessionState[sessionStateKey];
			}
			else
			{
				Graphs = new ObservableCollection<DataViewModel>();
			}
			sessionStateService.SessionState[sessionStateKey] = Graphs;
		}

		public ObservableCollection<DataViewModel> Graphs
		{
			get;
			private set;
		}
	}
}