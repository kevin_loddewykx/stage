﻿using System;

namespace StagePWO.Exceptions
{
	public class ImportException : Exception
	{
		public ImportException(string msg) : base(msg) { }
	}
}