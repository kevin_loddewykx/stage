﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace StagePWO.Common
{
	public class GridViewSelected : DependencyObject
	{

		public static readonly DependencyProperty SelectedItemsProperty =
			DependencyProperty.RegisterAttached("SelectedItems", typeof(IList<Object>),
			typeof(GridViewSelected),
			new PropertyMetadata(null));


		public static readonly DependencyProperty IsEnabledProperty =
			DependencyProperty.RegisterAttached("IsEnabled", typeof(bool),
			typeof(GridViewSelected),
			new PropertyMetadata(false, OnIsEnabledChanged));

		public static bool GetIsEnabled(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsEnabledProperty);
		}

		public static void SetIsEnabled(DependencyObject obj, bool value)
		{
			obj.SetValue(IsEnabledProperty, value);
		}

		private static void OnIsEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var txt = d as GridView;
			if (txt != null)
			{
				if ((bool)e.NewValue)
					txt.SelectionChanged += gridV_SelectionChanged;
				else
					txt.SelectionChanged -= gridV_SelectionChanged;
			}
		}

		public static IList<Object> GetSelectedItems(DependencyObject d)
		{
			return (IList<Object>)d.GetValue(SelectedItemsProperty);
		}

		public static void SetSelectedItems(DependencyObject d, IList<Object> value)
		{

			d.SetValue(SelectedItemsProperty, value);
		}

		private static void gridV_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var txt = sender as GridView;
			if (txt.GetValue(GridViewSelected.SelectedItemsProperty) == txt.SelectedItems)
			{
				IList<object> list = new ObservableCollection<object>();
				foreach (object obj in txt.SelectedItems)
				{
					list.Add(obj);
				}
				txt.SetValue(GridViewSelected.SelectedItemsProperty, list);
				return;
			}
			txt.SetValue(GridViewSelected.SelectedItemsProperty, txt.SelectedItems);
		}
	}
}