﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace StagePWO.Common
{
	public class GraphTemplateSelector : DataTemplateSelector
	{
		public DataTemplate NewGraph { get; set; }

		public DataTemplate LoadingGraph { get; set; }

		public DataTemplate ShowGraph { get; set; }

		protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
		{
			byte data = Convert.ToByte(item);
			if (data == 1)
			{
				return LoadingGraph;
			}
			else if (data == 2)
			{
				return ShowGraph;
			}
			return NewGraph;
		}
	}
}