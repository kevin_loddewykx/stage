﻿using System.Runtime.Serialization;

namespace StagePWO.Common
{
	[DataContract]
	public class CustomKeyValuePair
	{
		[DataMember]
		public int Key { get; set; }

		[DataMember]
		public string Value { get; set; }
	}
}