﻿using System;
using Windows.UI.Xaml.Data;

namespace StagePWO.Common
{
	public class GraphTemplateConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			GraphTemplateSelector graphSelc = parameter as GraphTemplateSelector;
			return graphSelc.SelectTemplate(value, null);
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new Exception();
		}
	}
}