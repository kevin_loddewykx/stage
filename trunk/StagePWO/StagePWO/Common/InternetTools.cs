﻿using Windows.Networking.Connectivity;

namespace InternetDetection
{
	public static class InternetTools
	{
		public delegate void InternetConnectionChangedHandler(bool args);

		public static event InternetConnectionChangedHandler InternetConnectionChanged;

		static InternetTools()
		{
			NetworkInformation.NetworkStatusChanged += NetworkInformation_NetworkStatusChanged;
		}

		private static void NetworkInformation_NetworkStatusChanged(object sender)
		{
			bool arg = IsConnectedToInternet();

			if (InternetConnectionChanged != null)
				InternetConnectionChanged(arg);
		}

		public static bool IsConnectedToInternet()
		{
			ConnectionProfile connectionProfile = NetworkInformation.GetInternetConnectionProfile();
			return (connectionProfile != null && connectionProfile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess);
		}
	}
}