﻿using OxyPlot;
using OxyPlot.Series;
using System.Collections.Generic;

namespace StagePWO.Common
{
	public class MyScatterSeries : ScatterSeries
	{
		/// <summary>
		/// Renders the series on the specified rendering context.
		/// </summary>
		/// <param name="rc">The rendering context.</param>
		/// <param name="model">The owner plot model.</param>
		public override void Render(IRenderContext rc, PlotModel model)
		{
			var actualPoints = this.ActualPoints;
			int n = actualPoints.Count;
			if (n == 0)
			{
				return;
			}

			var clippingRect = this.GetClippingRect();

			var allPoints = new List<ScreenPoint>(n);
			var allMarkerSizes = new List<double>(n);
			var selectedPoints = new List<ScreenPoint>();
			var selectedMarkerSizes = new List<double>(n);
			var groupPoints = new Dictionary<int, IList<ScreenPoint>>();
			var groupSizes = new Dictionary<int, IList<double>>();

			// check if any item of the series is selected
			bool isSelected = this.IsSelected();

			// Transform all points to screen coordinates
			for (int i = 0; i < n; i++)
			{
				var dp = new DataPoint(actualPoints[i].X, actualPoints[i].Y);
				double size = double.NaN;
				double value = double.NaN;

				var scatterPoint = actualPoints[i];
				if (scatterPoint != null)
				{
					size = scatterPoint.Size;
					value = scatterPoint.Value;
				}

				if (double.IsNaN(size))
				{
					size = this.MarkerSize;
				}

				// Transform from data to screen coordinates
				var screenPoint = this.XAxis.Transform(dp.X, dp.Y, this.YAxis);

				if (isSelected && this.IsItemSelected(i))
				{
					selectedPoints.Add(screenPoint);
					selectedMarkerSizes.Add(size);
					continue;
				}

				// This line has been changed
				if (this.ColorAxis != null && MarkerFill.Equals(OxyColors.Automatic))
				{
					if (double.IsNaN(value))
					{
						// The value is not defined, skip this point.
						continue;
					}

					int group = this.ColorAxis.GetPaletteIndex(value);
					if (!groupPoints.ContainsKey(group))
					{
						groupPoints.Add(group, new List<ScreenPoint>());
						groupSizes.Add(group, new List<double>());
					}

					groupPoints[group].Add(screenPoint);
					groupSizes[group].Add(size);
				}
				else
				{
					allPoints.Add(screenPoint);
					allMarkerSizes.Add(size);
				}
			}

			// Offset of the bins
			var binOffset = this.XAxis.Transform(this.MinX, this.MaxY, this.YAxis);

			rc.SetClip(clippingRect);

			if (this.ColorAxis != null)
			{
				// Draw the grouped (by color defined in ColorAxis) markers
				var markerIsStrokedOnly = this.MarkerType == MarkerType.Plus || this.MarkerType == MarkerType.Star || this.MarkerType == MarkerType.Cross;
				foreach (var group in groupPoints)
				{
					var color = this.ColorAxis.GetColor(group.Key);
					rc.DrawMarkers(
						clippingRect,
						group.Value,
						this.MarkerType,
						this.MarkerOutline,
						groupSizes[group.Key],
						color,
						markerIsStrokedOnly ? color : this.MarkerStroke,
						this.MarkerStrokeThickness,
						this.BinSize,
						binOffset);
				}
			}

			// Draw unselected markers
			rc.DrawMarkers(
					clippingRect,
					allPoints,
					this.MarkerType,
					this.MarkerOutline,
					allMarkerSizes,
					this.ActualMarkerFillColor,
					this.MarkerStroke,
					this.MarkerStrokeThickness,
					this.BinSize,
					binOffset);

			// Draw the selected markers
			rc.DrawMarkers(
				clippingRect,
				selectedPoints,
				this.MarkerType,
				this.MarkerOutline,
				selectedMarkerSizes,
				this.PlotModel.SelectionColor,
				this.PlotModel.SelectionColor,
				this.MarkerStrokeThickness,
				this.BinSize,
				binOffset);

			rc.ResetClip();
		}
	}
}