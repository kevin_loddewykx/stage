﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.StoreApps;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using Microsoft.Practices.Unity;
using StagePWO.Common;
using StagePWO.Model;
using StagePWO.Repository;
using StagePWO.Services;
using StagePWO.ViewModels;
using StagePWO.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.Storage;
using Windows.UI.ApplicationSettings;

namespace StagePWO
{
	sealed partial class App : MvvmAppBase
	{
		private readonly IUnityContainer _container = new UnityContainer();

		public App()
		{
			this.InitializeComponent();
		}

		protected override Task OnLaunchApplication(LaunchActivatedEventArgs args)
		{
			NavigationService.Navigate("Main", null);
			return Task.FromResult<object>(null);
		}

		protected override void OnInitialize(IActivatedEventArgs args)
		{
			_container.RegisterType<ICacheService, TemporaryFolderCacheService>(new ContainerControlledLifetimeManager());
			_container.RegisterInstance<IResourceLoader>(new ResourceLoaderAdapter(new ResourceLoader()));
			_container.RegisterInstance<IDropboxRepo>(new DropboxRepo(_container.Resolve<ICacheService>(), _container.Resolve<IResourceLoader>()));
			_container.RegisterInstance<IGraphsRepo>(new GraphsRepo(SessionStateService));
			_container.RegisterInstance<INavigationService>(NavigationService);
			_container.RegisterInstance<IEventAggregator>(new EventAggregator());
			_container.RegisterInstance<ISettings>(new Settings());
			if (!ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.ASK_ON_START))
			{
				ApplicationData.Current.LocalSettings.Values[DropboxHelperService.ASK_ON_START] = true;
			}
		}

		protected override object Resolve(Type type)
		{
			return _container.Resolve(type);
		}

		protected override void OnRegisterKnownTypesForSerialization()
		{
			base.OnRegisterKnownTypesForSerialization();
			SessionStateService.RegisterKnownType(typeof(ObservableCollection<DataViewModel>));
			SessionStateService.RegisterKnownType(typeof(ObservableCollection<object>));
			SessionStateService.RegisterKnownType(typeof(ObservableCollection<string>));
			SessionStateService.RegisterKnownType(typeof(MeasurementData));
			SessionStateService.RegisterKnownType(typeof(int[]));
			SessionStateService.RegisterKnownType(typeof(bool[]));
			SessionStateService.RegisterKnownType(typeof(List<CustomKeyValuePair>));
		}

		protected override IList<SettingsCommand> GetSettingsCommands()
		{
			IList<SettingsCommand> settingsCommands = new List<SettingsCommand>();
			IResourceLoader resourceLoader = _container.Resolve<IResourceLoader>();
			settingsCommands.Add(new SettingsCommand(Guid.NewGuid().ToString(), resourceLoader.GetString("aboutFlyoutTitle"), (c) => new AboutFlyout().Show()));
			settingsCommands.Add(new SettingsCommand(Guid.NewGuid().ToString(), resourceLoader.GetString("privacyPolicyFlyoutTitle"), (c) => new PrivacyPolicyFlyout().Show()));
			settingsCommands.Add(new SettingsCommand(Guid.NewGuid().ToString(), resourceLoader.GetString("dropboxFlyoutTitle"), (c) => new DropboxFlyout().Show()));

			return settingsCommands;
		}
	}
}