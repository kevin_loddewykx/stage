﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace StagePWO.Behaviors
{
	public class HighlightFormFieldOnErrors : Behavior<FrameworkElement>
	{
		public bool PropertyErrors
		{
			get { return (bool)GetValue(PropertyErrorsProperty); }
			set { SetValue(PropertyErrorsProperty, value); }
		}

		public static DependencyProperty PropertyErrorsProperty =
			DependencyProperty.RegisterAttached("PropertyErrors", typeof(bool), typeof(HighlightFormFieldOnErrors), new PropertyMetadata(false, OnPropertyErrorsChanged));

		private static void OnPropertyErrorsChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
		{
			if (args == null || args.NewValue == null)
			{
				return;
			}
			
			FrameworkElement control = ((Behavior<FrameworkElement>)d).AssociatedObject;
			Style style = null;
			if (control is ComboBox)
			{
				style = (bool)args.NewValue ? (Style)Application.Current.Resources["HighlightComboBoxStyle"] : null;
			}
			else
			{
				style = (bool)args.NewValue ? (Style)Application.Current.Resources["HighlightTextBoxStyle"] : null;
			}
			control.Style = style;
		}

		protected override void OnAttached()
		{
		}

		protected override void OnDetached()
		{
		}
	}
}