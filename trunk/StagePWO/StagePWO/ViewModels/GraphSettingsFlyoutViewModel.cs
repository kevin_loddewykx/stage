﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.StoreApps;
using StagePWO.Events;
using StagePWO.Model;

namespace StagePWO.ViewModels
{
	public class GraphSettingsFlyoutViewModel : BindableBase
	{
		private IEventAggregator _eventAggregator;
		private ISettings _settings;

		public GraphSettingsFlyoutViewModel(ISettings settings, IEventAggregator eventAggregator)
		{
			this._settings = settings;
			this._eventAggregator = eventAggregator;
		}

		public bool ContourOn
		{
			get { return _settings.ContourOn; }
			set
			{
				_settings.ContourOn = value;
				OnPropertyChanged("ContourOn");
				_eventAggregator.GetEvent<SettingsChangedEvent>().Publish(null);
			}
		}

		public int IntervalLevel
		{
			get { return _settings.IntervalLevel; }
			set
			{
				_settings.IntervalLevel = value;
				OnPropertyChanged("IntervalLevel");
				_eventAggregator.GetEvent<SettingsChangedEvent>().Publish(null);
			}
		}

		public bool ScatterOn
		{
			get { return _settings.ScatterOn; }
			set
			{
				_settings.ScatterOn = value;
				OnPropertyChanged("ScatterOn");
				_eventAggregator.GetEvent<SettingsChangedEvent>().Publish(null);
			}
		}
	}
}