﻿using ExcelParserWrapper;
using Microsoft.Practices.Prism.StoreApps;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using StagePWO.Model;
using StagePWO.Repository;
using StagePWO.Services;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml.Navigation;
using ExcelParserCore;
using StagePWO.Common;
using System.Collections.ObjectModel;
using StagePWO.Exceptions;

namespace StagePWO.ViewModels
{
	public class NewGraphPageViewModel : ViewModel
	{
		public static readonly string SpeedToken = "Speed";
		public static readonly string TorqueToken = "Torque";
		public static readonly string EfficiencyToken = "Efficiency";
		private Engine engine;
		private string speedFile;
		private string torqueFile;
		private string efficiencyFile;
		private string nominalSpeed;
		private string nominalTorque;
		private string eeLabel;
		private bool isRelative = true;
		private NumberFormatInfo formatInfo;
		private MeasurementData data;
		private bool filePickerOpen;
		private bool[] selectedType;
		private int[] tabs;
		private ObservableCollection<string> errors;
		private bool importing;

		private IResourceLoader _resourceLoader;
		private ISettings _settings;
		private IDropboxRepo _dropboxRepo;

		private List<CustomKeyValuePair> sheets;

		public ICommand SetSpeedFile { get; private set; }

		public ICommand LostFocus { get; private set; }

		public ICommand SetTorqueFile { get; private set; }

		public ICommand SetEfficiencyFile { get; private set; }

		public ICommand SelectionChange { get; private set; }

		public ICommand ImportGraph { get; private set; }

		public NewGraphPageViewModel(INavigationService navigationService, IResourceLoader resourceLoader, IGraphsRepo graphsRepo, ISettings settings, IDropboxRepo dropboxRepo)
		{
			this._resourceLoader = resourceLoader;
			this._settings = settings;
			this._dropboxRepo = dropboxRepo;
			this.importing = true;
			this.engine = new Engine();
			this.SelectedType = new bool[] { true, false, false };
			this.Tabs = new int[3];
			this.EELabel = "IE 1";
			this.formatInfo = new NumberFormatInfo();
			this.SetSpeedFile = new DelegateCommand(async () => { SpeedFile = await this.SelectCsvFile(SpeedToken); await this.SetSheets(); }, () => this.CanOpenFilePicker());
			this.SetTorqueFile = new DelegateCommand(async () => { TorqueFile = await this.SelectCsvFile(TorqueToken); }, () => this.CanOpenFilePicker());
			this.SetEfficiencyFile = new DelegateCommand(async () => { EfficiencyFile = await this.SelectCsvFile(EfficiencyToken); }, () => this.CanOpenFilePicker());
			this.SelectionChange = new DelegateCommand(RemoveFiles);
			this.ImportGraph = new DelegateCommand(async () => await this.Importing(navigationService, graphsRepo), CanImport);
			this.LostFocus = new DelegateCommand(CheckValidationErrors);
		}

		private bool CanImport()
		{
			if (importing)
			{
				importing = false;
				return true;
			}
			return false;
		}

		private async Task Importing(INavigationService navigationService, IGraphsRepo graphsRepo)
		{
			if (errors == null)
			{
				this.Errors = new ObservableCollection<string>(new string[] { null, null, null, null, null, null, null, null, null });
			}
			CheckValidationErrors();
			if (Errors.Where(error => error != null).Count() == 0)
			{
				MessageDialog dialog = null;
				try
				{
					if (selectedType[0])
					{
						data = await new CSVImporterService(_resourceLoader).Import(IsRelative, engine);
					}
					else if (selectedType[1])
					{
						data = await new ExcelImporterService(_resourceLoader, Tabs[0], Tabs[1], Tabs[2]).Import(IsRelative, engine);
					}
					else
					{
						data = await new ExcelImporterService(_resourceLoader).Import(IsRelative, engine);
					}
				}
				catch (UnauthorizedAccessException)
				{
					dialog = new MessageDialog(_resourceLoader.GetString("fileAccessError"), _resourceLoader.GetString("fileAccessErrorTitle"));
				}
				catch (ImportException e)
				{
					dialog = new MessageDialog(e.Message, _resourceLoader.GetString("importFailed"));
				}
				catch (Exception)
				{
					dialog = new MessageDialog(_resourceLoader.GetString("importFailedMsg"), _resourceLoader.GetString("importFailed"));
				}
				if (dialog != null)
				{
					await dialog.ShowAsync();
					return;
				}
				graphsRepo.Graphs[Position].Data = data;
				navigationService.GoBack();
			}
		}

		private void CheckValidationErrors()
		{
			if (Errors != null)
			{
				List<Engine.ErrorTags> errorKeys = engine.ValidateProperties();
				Errors[0] = errorKeys.Contains(Engine.ErrorTags.Speed) ? _resourceLoader.GetString("speedError") : null;
				Errors[1] = errorKeys.Contains(Engine.ErrorTags.Torque) ? _resourceLoader.GetString("torqueError") : null;
				Errors[2] = errorKeys.Contains(Engine.ErrorTags.EE) ? _resourceLoader.GetString("requiredField") : null;
				Errors[3] = !StorageApplicationPermissions.FutureAccessList.ContainsItem(SpeedToken) ? _resourceLoader.GetString("requiredField") : null;
				if (selectedType[1])
				{
					Errors[6] = (Tabs[0] == 0) ? _resourceLoader.GetString("requiredField") : null;
					Errors[7] = (Tabs[1] == 0) ? _resourceLoader.GetString("requiredField") : (Tabs[0] != 0 && Tabs[1] != 0 && Tabs[0] == Tabs[1]) ? _resourceLoader.GetString("tabError") : null;
					Errors[8] = (Tabs[2] == 0) ? _resourceLoader.GetString("requiredField") : (Tabs[1] != 0 && Tabs[2] != 0 && Tabs[1] == Tabs[2]) ? _resourceLoader.GetString("tabError") : (Tabs[0] != 0 && Tabs[2] != 0 && Tabs[0] == Tabs[2]) ? _resourceLoader.GetString("tabError") : null;
				}
				else
				{
					Errors[4] = !StorageApplicationPermissions.FutureAccessList.ContainsItem(TorqueToken) ? _resourceLoader.GetString("requiredField") : null;
					Errors[5] = !StorageApplicationPermissions.FutureAccessList.ContainsItem(EfficiencyToken) ? _resourceLoader.GetString("requiredField") : null;
				}
			}
		}

		private void RemoveFiles()
		{
			Sheets = null;
			if (StorageApplicationPermissions.FutureAccessList.ContainsItem(SpeedToken))
			{
				StorageApplicationPermissions.FutureAccessList.Remove(SpeedToken);
				SpeedFile = "";
			}
			if (StorageApplicationPermissions.FutureAccessList.ContainsItem(TorqueToken))
			{
				StorageApplicationPermissions.FutureAccessList.Remove(TorqueToken);
				TorqueFile = "";
			}
			if (StorageApplicationPermissions.FutureAccessList.ContainsItem(EfficiencyToken))
			{
				StorageApplicationPermissions.FutureAccessList.Remove(EfficiencyToken);
				EfficiencyFile = "";
			}
			Errors = null;
		}

		[RestorableState]
		public ObservableCollection<string> Errors { get { return errors; } set { SetProperty(ref errors, value); } }

		private bool CanOpenFilePicker()
		{
			return !filePickerOpen;
		}

		// Needed to implement workaround for the following
		// Bug: https://connect.microsoft.com/VisualStudio/feedback/details/750518/targetexception-error-in-binding-to-dictionary-string-object
		// Workaround: http://social.msdn.microsoft.com/Forums/windowsapps/en-US/34a8dbf6-8372-4912-9c60-74d9c89bdb15/binding-combobox-using-dictionary
		[RestorableState]
		public List<CustomKeyValuePair> Sheets
		{
			get { return sheets; }
			set { SetProperty(ref sheets, value); }
		}

		[RestorableState]
		public int[] Tabs
		{
			get { return tabs; }
			set { SetProperty(ref tabs, value); }
		}

		[RestorableState]
		public bool[] SelectedType
		{
			get { return selectedType; }
			set { SetProperty(ref selectedType, value); }
		}

		[RestorableState]
		public bool IsRelative { get { return isRelative; } set { SetProperty(ref isRelative, value); } }

		private async Task<string> SelectCsvFile(String token)
		{
			filePickerOpen = true;
			// Create OpenFileDialog 
			FileOpenPicker picker = new FileOpenPicker();

			// Set filter for file extension and default file extension 
			picker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
			if (selectedType[0])
			{
				picker.FileTypeFilter.Add(".csv");
			}
			else
			{
				picker.FileTypeFilter.Add(".xlsx");
			}

			StorageFile file = await picker.PickSingleFileAsync();

			if (file != null)
			{
				StorageApplicationPermissions.FutureAccessList.AddOrReplace(token, file);
				filePickerOpen = false;
				return file.Path;
			}
			if (StorageApplicationPermissions.FutureAccessList.ContainsItem(token))
			{
				StorageApplicationPermissions.FutureAccessList.Remove(token);
			}
			filePickerOpen = false;
			return "";
		}

		[RestorableState]
		public string SpeedFile
		{
			get
			{
				return speedFile;
			}
			set
			{
				SetProperty(ref speedFile, value);
			}
		}

		public async Task SetSheets()
		{
			Sheets = null;
			if (selectedType[1] && StorageApplicationPermissions.FutureAccessList.ContainsItem(SpeedToken))
			{
				bool hasError = false;
				try
				{
					using (IRandomAccessStream dataStream = await (await StorageApplicationPermissions.FutureAccessList.GetFileAsync(SpeedToken)).OpenReadAsync())
					{
						IZipArchive dataZipArchive = new ZipArchiveWrapper(dataStream.AsStream());
						ExcelParser dataParser = new ExcelParser(dataZipArchive);
						Dictionary<int, string> sheets = dataParser.GetSheets();
						if (sheets.Count > 2)
						{
							Sheets = sheets.Select(kvp => new CustomKeyValuePair { Key = kvp.Key, Value = kvp.Value }).ToList<CustomKeyValuePair>();
						}
					}
				}
				catch (UnauthorizedAccessException)
				{
					
					hasError = true;
				}
				if (hasError)
				{
					MessageDialog dialog = new MessageDialog(_resourceLoader.GetString("fileAccessError"), _resourceLoader.GetString("fileAccessErrorTitle"));
					await dialog.ShowAsync();
					if (StorageApplicationPermissions.FutureAccessList.ContainsItem(SpeedToken))
					{
						StorageApplicationPermissions.FutureAccessList.Remove(SpeedToken);
						SpeedFile = "";
					}
				}
			}
		}

		[RestorableState]
		public string TorqueFile
		{
			get
			{
				return torqueFile;
			}
			set
			{
				SetProperty(ref torqueFile, value);
			}
		}

		[RestorableState]
		public string EfficiencyFile
		{
			get
			{
				return efficiencyFile;
			}
			set
			{
				SetProperty(ref efficiencyFile, value);
			}
		}

		[RestorableState]
		public string NominalSpeed
		{
			get { return nominalSpeed; }
			set
			{
				int e = 0;
				int.TryParse(value, out e);

				engine.NominalSpeed = e;
				SetProperty(ref nominalSpeed, value);
			}
		}

		[RestorableState]
		public string NominalTorque
		{
			get { return nominalTorque; }
			set
			{
				double e = 0;
				formatInfo.NumberDecimalSeparator = ",";
				if (!(double.TryParse(value, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite, formatInfo, out e)))
				{
					formatInfo.NumberDecimalSeparator = ".";
					double.TryParse(value, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite, formatInfo, out e);
				}
				engine.NominalTorque = e;
				SetProperty(ref nominalTorque, value);
			}
		}

		[RestorableState]
		public string EELabel
		{
			get { return eeLabel; }
			set
			{
				engine.EELabel = value;
				SetProperty(ref eeLabel, value);
			}
		}

		[RestorableState]
		public string ExtraInfo
		{
			get { return engine.ExtraInfo; }
			set
			{
				engine.ExtraInfo = value;
				OnPropertyChanged("ExtraInfo");
			}
		}

		[RestorableState]
		public byte Position { get; set; }

		public override void OnNavigatedTo(object navigationParameter, NavigationMode navigationMode, Dictionary<string, object> viewModelState)
		{
			base.OnNavigatedTo(navigationParameter, navigationMode, viewModelState);
			Position = Convert.ToByte(navigationParameter);
			if (navigationMode == NavigationMode.New)
			{
				RemoveFiles();
			}
		}

		public override void OnNavigatedFrom(Dictionary<string, object> viewModelState, bool suspending)
		{
			base.OnNavigatedFrom(viewModelState, suspending);
			if (!suspending)
			{
				RemoveFiles();
			}
		}
	}
}