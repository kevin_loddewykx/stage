﻿using Microsoft.Practices.Prism.StoreApps;
using StagePWO.Repository;
using StagePWO.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;

namespace StagePWO.ViewModels
{
	public class DropboxFlyoutViewModel : BindableBase
	{
		private bool showBroker;
		private IDropboxRepo _dropboxRepo;
		private bool isResetting;

		public DropboxFlyoutViewModel(IDropboxRepo dropboxRepo) {
			this._dropboxRepo = dropboxRepo;
			isResetting = false;
			showBroker = (bool)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.ASK_ON_START]; 
			this.ResetDropbox = new DelegateCommand(async () => await ShowResetDialog(), IsResetting);
		}

		private async Task ShowResetDialog()
		{
			isResetting = true;
			await DropboxHelperService.ShowDropboxServiceBroker(_dropboxRepo);
			isResetting = false;
		}

		private bool IsResetting()
		{
			return !isResetting;
		}

		public ICommand ResetDropbox { get; private set; }

		public bool ShowBroker 
		{ 
			get {return showBroker; } 
			set 
			{ 
				ApplicationData.Current.LocalSettings.Values[DropboxHelperService.ASK_ON_START] = value; 
				SetProperty(ref showBroker, value);
			}
		}
	}
}