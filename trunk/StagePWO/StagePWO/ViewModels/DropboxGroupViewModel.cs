﻿using StagePWO.Common;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace StagePWO.ViewModels
{
	[DataContract]
	public class DropboxGroupViewModel : BindableBaseContracted
	{
		private ObservableCollection<DropboxDataViewModel> graphList;
		public DropboxGroupViewModel(DateTime year)
		{
			this.GraphList = new ObservableCollection<DropboxDataViewModel>();
			this.Date = year;
		}

		[DataMember]
		public ObservableCollection<DropboxDataViewModel> GraphList { get { return graphList; } set { SetProperty(ref graphList, value); } }

		[DataMember]
		public DateTime Date { get; private set; }
	}
}