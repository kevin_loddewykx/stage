﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.StoreApps;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using OxyPlot.Metro;
using StagePWO.Events;
using StagePWO.Model;
using StagePWO.Repository;
using StagePWO.Services;
using StagePWO.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.ApplicationSettings;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace StagePWO.ViewModels
{
	public class MainPageViewModel : ViewModel
	{
		private IList<object> selectedItems;
		private int graphCount;

		private IResourceLoader _resourceLoader;
		private ISettings _settings;
		private IGraphsRepo _graphsRepo;
		private IDropboxRepo _dropboxRepo;
		private IEventAggregator _eventAggregator;

		private DataTransferManager dataTransferManager;
		private SettingsPane settingsPane;

		private bool isExporting;
		private bool graphSelected;


		public ICommand ResetGraphViews { get; private set; }

		public ICommand ExportGraphViews { get; private set; }

		public MainPageViewModel(IResourceLoader resourceLoader, IGraphsRepo graphsRepo, ISettings settings, IEventAggregator eventAggregator, IDropboxRepo dropboxRepo)
		{
			_resourceLoader = resourceLoader;
			_settings = settings;
			_dropboxRepo = dropboxRepo;
			_graphsRepo = graphsRepo;
			_eventAggregator = eventAggregator;

			isExporting = false;
			graphSelected = false;

			if (graphsRepo.Graphs.Count == 0)
			{
				graphsRepo.Graphs.Add(new DataViewModel(0, _dropboxRepo));
			}
			eventAggregator.GetEvent<SettingsChangedEvent>().Subscribe((obj) => UpdatePlots(obj));
			eventAggregator.GetEvent<GraphInitializedEvent>().Subscribe((obj) => UpdateImageHandlers());
			ResetGraphViews = new DelegateCommand(ResetGraphs);
			ExportGraphViews = new DelegateCommand(ExportGraphs, CanExport);
			UpdateFields();

			dataTransferManager = DataTransferManager.GetForCurrentView();
			settingsPane = SettingsPane.GetForCurrentView();
			settingsPane.CommandsRequested += new TypedEventHandler<SettingsPane, SettingsPaneCommandsRequestedEventArgs>(addSettingsFlyout);
			SelectedItems = new ObservableCollection<object>();
		}

		private bool CanExport()
		{
			return !isExporting && graphSelected ;
		}

		public override async void OnNavigatedTo(object navigationParameter, NavigationMode navigationMode, Dictionary<string, object> viewModelState)
		{
			base.OnNavigatedTo(navigationParameter, navigationMode, viewModelState);
			if (navigationMode == NavigationMode.New && !ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && (bool)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.ASK_ON_START])
			{
				await DropboxHelperService.ShowDropboxServiceBroker(_dropboxRepo);
			}
			foreach (DataViewModel graph in Graphs)
			{
				await CoreWindow.GetForCurrentThread().Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => await graph.CreateGraphModel(_resourceLoader, _settings, _dropboxRepo, _eventAggregator));
			}
		}

		private void addSettingsFlyout(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
		{
			args.Request.ApplicationCommands.Add(new SettingsCommand(Guid.NewGuid().ToString(), _resourceLoader.GetString("settingsFlyoutTitle"), (c) => new GraphSettingsFlyout().Show()));
		}

		private async void ImageShare(DataTransferManager sender, DataRequestedEventArgs args)
		{
			DataRequest request = args.Request;
			request.Data.Properties.Title = _resourceLoader.GetString("emailTitle");
			request.Data.Properties.Description = _resourceLoader.GetString("emailDescription");

			DataRequestDeferral deferral = request.GetDeferral();

			try
			{
				List<IStorageItem> storageItems = new List<IStorageItem>();

				float logicalDpi = DisplayInformation.GetForCurrentView().LogicalDpi;
				foreach (DataViewModel graph in SelectedItems)
				{
					if (graph.PlotModel != null && graph.PlotModel.PlotView != null)
					{
						StorageFolder temporaryFolder = ApplicationData.Current.TemporaryFolder;
						StorageFile storageFile = await temporaryFolder.CreateFileAsync("S" + graph.Data.Engine.NominalSpeed + "_T" + graph.Data.Engine.NominalTorque + "_IE" + graph.Data.Engine.EELabel + ".png", CreationCollisionOption.GenerateUniqueName);

						RenderTargetBitmap targetBitmap = new RenderTargetBitmap();
						await targetBitmap.RenderAsync((PlotView)graph.PlotModel.PlotView);
						using (Stream stream = await storageFile.OpenStreamForWriteAsync())
						{
							// Initialization.  
							IBuffer pixelBuffer = await targetBitmap.GetPixelsAsync();

							// convert stream to IRandomAccessStream  
							IRandomAccessStream randomAccessStream = stream.AsRandomAccessStream();
							// encoding to PNG  
							BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, randomAccessStream);
							// Finish saving  
							encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, (uint)targetBitmap.PixelWidth,
									   (uint)targetBitmap.PixelHeight, logicalDpi, logicalDpi, WindowsRuntimeBufferExtensions.ToArray(pixelBuffer));
							// Flush encoder.  
							await encoder.FlushAsync();
						}
						storageItems.Add(storageFile);
					}
				}
				if (storageItems.Count > 0)
				{
					request.Data.SetStorageItems(storageItems);
				}
			}
			finally
			{
				deferral.Complete();
			}
		}

		private void UpdatePlots(object notUsed)
		{
			foreach (DataViewModel data in Graphs)
			{
				data.UpdatePlot();
			}
		}

		public IList<object> SelectedItems
		{
			get
			{
				return selectedItems;
			}
			set
			{
				SetProperty(ref selectedItems, value);
				UpdateImageHandlers();
			}
		}

		private void UpdateImageHandlers()
		{
			dataTransferManager.DataRequested -= new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(ImageShare);
			graphSelected = false;
			foreach (DataViewModel graph in SelectedItems)
			{
				if (graph.PlotModel != null && graph.PlotModel.PlotView != null)
				{
					dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(ImageShare);
					graphSelected = true;
					break;
				}
			}
			((DelegateCommand)ExportGraphViews).RaiseCanExecuteChanged();
		}

		public bool RemoveGraphs()
		{
			bool hasRemoved = false;
			byte counter = 0;
			for (byte i = 0; i < GraphCount; ++i)
			{
				if (SelectedItems.Contains(Graphs[i - counter]))
				{
					hasRemoved = true;
					Graphs.RemoveAt(i - counter);
					++counter;
				}
				else
				{
					Graphs[i - counter].Position -= counter;
				}
			}
			if (Graphs.Count == 0)
			{
				Graphs.Add(new DataViewModel(0, _dropboxRepo));
			}
			UpdateFields();
			return hasRemoved;
		}

		private void ResetGraphs()
		{
			foreach (DataViewModel graph in SelectedItems)
			{
				if (graph.PlotModel != null && graph.PlotModel.PlotView != null)
				{
					((PlotView)graph.PlotModel.PlotView).Model = null;
				}
				graph.Reset();
			}
			UpdateImageHandlers();
		}

		public ObservableCollection<DataViewModel> Graphs
		{
			get { return _graphsRepo.Graphs; }
		}

		public bool AddGraph()
		{
			if (Graphs.Count < 4)
			{
				Graphs.Add(new DataViewModel(Convert.ToByte(Graphs.Count), _dropboxRepo));
				UpdateFields();
				return true;
			}
			return false;
		}

		private void UpdateFields()
		{
			GraphCount = Graphs.Count;
		}

		[RestorableState]
		public int GraphCount
		{
			get { return graphCount; }
			private set { SetProperty(ref graphCount, value); }
		}

		// Nullify the reference from the PlotControl on the view to the PlotModel
		private void CleanUpPlotReferences()
		{
			foreach (DataViewModel graph in Graphs)
			{
				if (graph.PlotModel != null && graph.PlotModel.PlotView != null)
				{
					((PlotView)graph.PlotModel.PlotView).Model = null;
				}
			}
		}

		public override void OnNavigatedFrom(System.Collections.Generic.Dictionary<string, object> viewModelState, bool suspending)
		{
			base.OnNavigatedFrom(viewModelState, suspending);
			if (!suspending)
			{
				settingsPane.CommandsRequested -= new TypedEventHandler<SettingsPane, SettingsPaneCommandsRequestedEventArgs>(addSettingsFlyout);
				CleanUpPlotReferences();
			}
		}

		public async void ExportGraphs()
		{
			isExporting = true;

			FolderPicker picker = new FolderPicker();
			picker.ViewMode = PickerViewMode.Thumbnail;
			picker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
			picker.FileTypeFilter.Add("*");
			StorageFolder folder = await picker.PickSingleFolderAsync();
			if (folder != null)
			{
				float logicalDpi = DisplayInformation.GetForCurrentView().LogicalDpi;
				foreach (DataViewModel graph in SelectedItems)
				{
					if (graph.PlotModel != null && graph.PlotModel.PlotView != null)
					{
						StorageFile file = await folder.CreateFileAsync("S" + graph.Data.Engine.NominalSpeed + "_T" + graph.Data.Engine.NominalTorque + "_IE" + graph.Data.Engine.EELabel + ".png", CreationCollisionOption.GenerateUniqueName);
						RenderTargetBitmap targetBitmap = new RenderTargetBitmap();
						await targetBitmap.RenderAsync((PlotView)graph.PlotModel.PlotView);

						using (Stream stream = await file.OpenStreamForWriteAsync())
						{
							// Initialization.  
							IBuffer pixelBuffer = await targetBitmap.GetPixelsAsync();

							// convert stream to IRandomAccessStream  
							IRandomAccessStream randomAccessStream = stream.AsRandomAccessStream();
							// encoding to PNG  
							BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, randomAccessStream);
							// Finish saving  
							encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, (uint)targetBitmap.PixelWidth,
									   (uint)targetBitmap.PixelHeight, logicalDpi, logicalDpi, WindowsRuntimeBufferExtensions.ToArray(pixelBuffer));
							// Flush encoder.  
							await encoder.FlushAsync();
						}
					}
				}
			}
			isExporting = false;	
		}
	}
}