﻿using DropNetRT;
using Microsoft.Practices.Prism.StoreApps;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using StagePWO.Common;
using StagePWO.Model;
using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using System.IO;
using OxyPlot.Metro;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Graphics.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Xml;
using Windows.UI.Popups;
using StagePWO.Services;
using StagePWO.Repository;
using DropNetRT.Exceptions;
using System.Windows.Input;
using InternetDetection;
using Microsoft.Practices.Prism.PubSubEvents;
using StagePWO.Events;

namespace StagePWO.ViewModels
{
	[DataContract]
	public class DataViewModel : BindableBaseContracted
	{
		private IResourceLoader _resourceLoader;
		private ISettings _settings;
		private PlotModel plotModel;

		private CancellationTokenSource tokenSource;

		private MeasurementData data;
		private byte position;
		private bool importHasStarted;

		private HeatMapSeries hmSeries;
		private int activeDataTemplate;

		private IDropboxRepo _dropboxRepo;
		private IEventAggregator _eventAggregator;

		private bool uploadedImage = false;

		private DropboxDataViewModel dbData;

		public ICommand UpdateImage { get; private set; }

		public DataViewModel(byte position, IDropboxRepo dropboxRepo)
		{
			this.Position = position;
			this._dropboxRepo = dropboxRepo;
		}

		public PlotModel PlotModel
		{
			get { return plotModel; }
			set { SetProperty(ref plotModel, value); }
		}

		public void Reset()
		{
			if (tokenSource != null)
			{
				tokenSource.Cancel();
			}
			plotModel = null;
			data = null;
			hmSeries = null;
			importHasStarted = false;
			ActiveDataTemplate = 0;
		}

		public async Task Import()
		{
			try
			{
				await UploadOriginalPoints();
				tokenSource = new CancellationTokenSource();
				await Data.Initialize(tokenSource.Token);
				await UploadCalculatedPoints();
			}
			catch (OperationCanceledException)
			{
				// Graph got resetted or removed
			}
		}

		private async Task UploadOriginalPoints()
		{
			if (string.IsNullOrWhiteSpace(FolderId) && InternetTools.IsConnectedToInternet() && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_SECRET))
			{
				try
				{
					DropNetClient client = new DropNetClient(DropboxHelperService.APP_TOKEN, DropboxHelperService.APP_SECRET, (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_TOKEN], (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_SECRET]);
					client.UseSandbox = true;

					FolderId = Guid.NewGuid().ToString();
					await client.CreateFolder(FolderId);

					DateTime date = DateTime.Now;
					using (InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream())
					{
						XmlWriterSettings xws = new XmlWriterSettings();
						xws.Async = true;
						XmlWriter info = XmlWriter.Create((stream.AsStream()), xws);

						await info.WriteStartDocumentAsync();
						await info.WriteStartElementAsync("", DropboxHelperService.XML_ROOT, "");
						await info.WriteElementStringAsync("", DropboxHelperService.XML_DATE, "", date.ToString());
						await info.WriteElementStringAsync("", DropboxHelperService.XML_SPEED, "", "" + Data.Engine.NominalSpeed);
						await info.WriteElementStringAsync("", DropboxHelperService.XML_TORQUE, "", "" + Data.Engine.NominalTorque);
						await info.WriteElementStringAsync("", DropboxHelperService.XML_EFFICIENCY, "", Data.Engine.EELabel);
						await info.WriteElementStringAsync("", DropboxHelperService.XML_EXTRA, "", Data.Engine.ExtraInfo);
						await info.WriteEndDocumentAsync();
						await info.FlushAsync();
						stream.Seek(0);
						await client.Upload(FolderId, DropboxHelperService.DATA_NAME, stream.AsStream());
					}

					StreamWriter speed = new StreamWriter(new InMemoryRandomAccessStream().AsStream());
					StreamWriter torque = new StreamWriter(new InMemoryRandomAccessStream().AsStream());
					StreamWriter efficiency = new StreamWriter(new InMemoryRandomAccessStream().AsStream());

					double[,] streamData = Data.RBFData;

					for (int i = 0; i < Data.TripletRows; ++i)
					{
						for (int j = 0; j < Data.TripletColumns; ++j)
						{
							await speed.WriteAsync("" + streamData[i * Data.TripletRows + j, 0]);
							await torque.WriteAsync("" + streamData[i * Data.TripletRows + j, 1]);
							await efficiency.WriteAsync("" + streamData[i * Data.TripletRows + j, 2]);
							if (j < Data.TripletColumns - 1)
							{
								await speed.WriteAsync(";");
								await torque.WriteAsync(";");
								await efficiency.WriteAsync(";");
							}
						}
						await speed.WriteLineAsync();
						await torque.WriteLineAsync();
						await efficiency.WriteLineAsync();
					}
					await speed.FlushAsync();
					await torque.FlushAsync();
					await efficiency.FlushAsync();
					speed.BaseStream.Position = 0;
					torque.BaseStream.Position = 0;
					efficiency.BaseStream.Position = 0;
					await client.Upload(FolderId, DropboxHelperService.SPEED_NAME, speed.BaseStream);
					await client.Upload(FolderId, DropboxHelperService.TORQUE_NAME, torque.BaseStream);
					await client.Upload(FolderId, DropboxHelperService.EFFICIENCY_NAME, efficiency.BaseStream);

					dbData = new DropboxDataViewModel();
					dbData.Data = Data;
					dbData.FolderId = FolderId;
					dbData.Date = date;
					dbData.Title = "(" + date.ToString("dd/MM/yyyy") + ") " + _resourceLoader.GetString("speedTitle") + Data.Engine.NominalSpeed + " " + _resourceLoader.GetString("torqueTitle") + Data.Engine.NominalTorque + " " + _resourceLoader.GetString("eeLabelTitle") + Data.Engine.EELabel;
					await _dropboxRepo.AddAdded(dbData, date);
				}
				catch (Exception)
				{
				}
			}
		}

		public void UpdatePlot(bool firstRun = false)
		{
			if (ActiveDataTemplate == 2)
			{
				PlotModel.Series.Clear();
				PlotModel.Series.Add(hmSeries);

				if (_settings.ContourOn)
				{
					double[] intervals = new double[(int)Math.Ceiling(100.0 / _settings.IntervalLevel) - 1];
					int counter = 0;
					for (int i = _settings.IntervalLevel; i < 100; i += _settings.IntervalLevel)
					{
						intervals[counter++] = i;
					}
					// Need to take a new instance otherwise contourlevels won't update
					ContourSeries contourSeries = new ContourSeries
					{
						Color = OxyColors.Black,
						// ContourLevelStep <- Doesn't work 
						ContourLevels = intervals,
						ColumnCoordinates = Data.XValues,
						RowCoordinates = Data.YValues,
						Data = Data.Values,
					};
					PlotModel.Series.Add(contourSeries);
				}
				if (_settings.ScatterOn)
				{
					ScatterSeries ss = new MyScatterSeries
					{
						ItemsSource = Data.GetScatterPoints(),
						MarkerFill = OxyColors.Black,
						MarkerSize = 2,
						MarkerType = MarkerType.Circle,
						Mapping = item => new ScatterPoint(((EETriplet)item).Speed, ((EETriplet)item).Torque, double.NaN, ((EETriplet)item).Efficiency)
					};
					PlotModel.Series.Add(ss);
				}
				PlotModel.InvalidatePlot(true);
			}
		}

		private void SetUpModel()
		{
			PlotModel = new PlotModel();
			PlotModel.Title = _resourceLoader.GetString("speedTitle") + Data.Engine.NominalSpeed + "\t" + _resourceLoader.GetString("torqueTitle") + Data.Engine.NominalTorque + "\t" + _resourceLoader.GetString("eeLabelTitle") + Data.Engine.EELabel;
			PlotModel.Subtitle = Data.Engine.ExtraInfo;
			LinearAxis speedAxis = new LinearAxis
			{
				Position = AxisPosition.Bottom,
				Title = _resourceLoader.GetString("speedAxis"),
				Minimum = 0.0,
				Maximum = 2.0,
				AbsoluteMaximum = 5,
				AbsoluteMinimum = -3,
				StringFormat = "0.##",
				MinimumRange = 0.5,

			};
			LinearAxis torqueAxis = new LinearAxis
			{
				Position = AxisPosition.Left,
				Title = _resourceLoader.GetString("torqueAxis"),
				Minimum = 0.0,
				IntervalLength = 40,
				Maximum = 1.5,
				MinimumRange = 0.5,
				AbsoluteMaximum = 5,
				StringFormat = "0.##",
				AbsoluteMinimum = -3.5,

			};
			LinearColorAxis colorAxis = new LinearColorAxis
			{
				Position = AxisPosition.Right,
				InvalidNumberColor = OxyColors.Transparent,
				HighColor = OxyColors.Gray,
				Maximum = 100,
				Minimum = 0,
				LowColor = OxyColors.Black,
			};
			PlotModel.Axes.Add(colorAxis);
			PlotModel.Axes.Add(speedAxis);
			PlotModel.Axes.Add(torqueAxis);
		}

		public async Task CreateGraphModel(IResourceLoader resourceLoader, ISettings settings, IDropboxRepo dropboxRepo, IEventAggregator eventAggregator)
		{
			if (Data == null)
			{
				return;
			}
			_resourceLoader = resourceLoader;
			_settings = settings;
			_dropboxRepo = dropboxRepo;
			_eventAggregator = eventAggregator;
			UpdateImage = new DelegateCommand(async () => await UploadImage());

			if (Data.Values == null && !importHasStarted)
			{
				ActiveDataTemplate = 1;
				importHasStarted = true;
				bool hasFailed = false;
				try
				{
					await Import();
				}
				catch (alglib.alglibexception)
				{
					hasFailed = true;
				}
				if (hasFailed)
				{
					MessageDialog dialog = new MessageDialog(_resourceLoader.GetString("loadingError"), _resourceLoader.GetString("loadingFailed"));
					await dialog.ShowAsync();
					Reset();
					return;
				}
			}
			if (Data.Values != null)
			{
				if (PlotModel == null)
				{
					SetUpModel();
					hmSeries = new HeatMapSeries
					{
						X0 = 0,
						X1 = 2,
						Y0 = 0,
						Y1 = 1.5,
						Data = Data.Values,
						TrackerFormatString = _resourceLoader.GetString("seriesSpeedLabel") + " {2:0.00}\n" + _resourceLoader.GetString("seriesTorqueLabel") + " {4:0.00}\n" + _resourceLoader.GetString("seriesEfficiencyLabel") + " {5:0.00}"
					};
					ActiveDataTemplate = 2;
					UpdatePlot();
				}
			}
		}

		private async Task UploadCalculatedPoints()
		{
			if (InternetTools.IsConnectedToInternet() && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_SECRET))
			{
				// Maybe the internet wasn't active when importing started
				await UploadOriginalPoints();
				if (!string.IsNullOrEmpty(FolderId))
				{
					try
					{
						DropNetClient client = new DropNetClient(DropboxHelperService.APP_TOKEN, DropboxHelperService.APP_SECRET, (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_TOKEN], (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_SECRET]);
						client.UseSandbox = true;
						using (StreamWriter stream = new StreamWriter(new MemoryStream()))
						{
							double[] streamData = Data.ValuesFlat;
							int rowCount = streamData.Length;
							for (int i = 0; i < rowCount; ++i)
							{
								await stream.WriteLineAsync("" + streamData[i]);
							}
							await stream.FlushAsync();
							stream.BaseStream.Position = 0;
							await client.Upload(FolderId, DropboxHelperService.CALCULATED_NAME, stream.BaseStream);
							await _dropboxRepo.SaveData();
						}
					}
					catch (DropboxException) { }
				}
			}
		}

		public async Task UploadImage()
		{
			if (!uploadedImage && PlotModel != null && PlotModel.PlotView != null)
			{
				if (_eventAggregator != null)
				{
					_eventAggregator.GetEvent<GraphInitializedEvent>().Publish(null);
				}
				if (!string.IsNullOrEmpty(FolderId) && InternetTools.IsConnectedToInternet() && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_SECRET))
				{
					try
					{
						using (IRandomAccessStream randomAccessStream = new InMemoryRandomAccessStream())
						{
							RenderTargetBitmap targetBitmap = new RenderTargetBitmap();

							// The UIElement/Canvas used by RenderTargetBitmap needs to be visible on the screen to work,
							// otherwise a FormatException will be thrown.
							await targetBitmap.RenderAsync((PlotView)PlotModel.PlotView, 250, 250);
							IBuffer pixelBuffer = await targetBitmap.GetPixelsAsync();
							if (pixelBuffer.Length == 0)
							{
								return;
							}
							BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, randomAccessStream);
							byte[] imageData = WindowsRuntimeBufferExtensions.ToArray(pixelBuffer);
							encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, (uint)targetBitmap.PixelWidth,
								(uint)targetBitmap.PixelHeight, 92, 92, imageData);
							await encoder.FlushAsync();
							randomAccessStream.Seek(0);

							DropNetClient client = new DropNetClient(DropboxHelperService.APP_TOKEN, DropboxHelperService.APP_SECRET, (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_TOKEN], (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_SECRET]);
							client.UseSandbox = true;
							if (dbData != null)
							{
								byte[] bytes = new byte[randomAccessStream.Size];
								await randomAccessStream.ReadAsync(bytes.AsBuffer(), (uint)randomAccessStream.Size, InputStreamOptions.None);
								dbData.ImageData = bytes;
								dbData.InitImage();
								await _dropboxRepo.SaveData();
								randomAccessStream.Seek(0);
							}
							await client.Upload(FolderId, DropboxHelperService.IMAGE_NAME, randomAccessStream.AsStream());
							uploadedImage = true;
						}
					}
					catch (DropboxException) { }
					catch { }
				}
			}
		}

		[DataMember]
		public MeasurementData Data { get { return data; } set { SetProperty(ref data, value); } }

		[DataMember]
		public byte Position { get { return position; } set { SetProperty(ref position, value); } }

		[DataMember]
		public string FolderId { get; set; }

		[DataMember]
		public int ActiveDataTemplate
		{
			get { return activeDataTemplate; }
			set { SetProperty(ref activeDataTemplate, value); }
		}
	}
}