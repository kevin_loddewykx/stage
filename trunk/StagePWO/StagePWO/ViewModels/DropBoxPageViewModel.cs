﻿using DropNetRT;
using DropNetRT.Exceptions;
using InternetDetection;
using Microsoft.Practices.Prism.StoreApps;
using Microsoft.Practices.Prism.StoreApps.Interfaces;
using StagePWO.Model;
using StagePWO.Repository;
using StagePWO.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace StagePWO.ViewModels
{
	public class DropboxPageViewModel : ViewModel
	{
		private IList<object> selectedItems;

		public ICommand DeleteClick { get; private set; }
		public ICommand ItemClick { get; private set; }

		private IResourceLoader _resourceLoader;
		private ICacheService _cacheService;
		private ISettings _settings;
		private IDropboxRepo _dropboxRepo;

		public DropboxPageViewModel(IResourceLoader resourceLoader, INavigationService navigationService, IGraphsRepo graphsRepo, ICacheService cacheService, ISettings settings, IDropboxRepo dropboxRepo)
		{
			this._resourceLoader = resourceLoader;
			this._cacheService = cacheService;
			this._settings = settings;
			DropboxRepo = dropboxRepo;
			ItemClick = new DelegateCommand<ItemClickEventArgs>((data) => Importing(data, navigationService, graphsRepo));
			DeleteClick = new DelegateCommand(async () => await DeleteGraphs());
		}

		private async Task DeleteGraphs()
		{
			IList<object> selectedItems = new List<object>(SelectedItems);
			if (selectedItems == null)
			{
				return;
			}
			_dropboxRepo.AddDeleted(selectedItems.Select((x) => ((DropboxDataViewModel)x).FolderId));

			if (InternetTools.IsConnectedToInternet() && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_SECRET))
			{
				DropNetClient client = new DropNetClient(DropboxHelperService.APP_TOKEN, DropboxHelperService.APP_SECRET, (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_TOKEN], (string)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.USER_SECRET]);
				client.UseSandbox = true;
				foreach (object obj in selectedItems)
				{
					DropboxDataViewModel dbData = (DropboxDataViewModel)obj;
					try
					{
						await client.Delete(dbData.FolderId);
					}
					catch (DropboxException) { }
				}
			}
		}

		public IDropboxRepo DropboxRepo
		{
			get { return _dropboxRepo; }
			private set { SetProperty(ref _dropboxRepo, value); }
		}

		public override async void OnNavigatedTo(object navigationParameter, Windows.UI.Xaml.Navigation.NavigationMode navigationMode, Dictionary<string, object> viewModelState)
		{
			base.OnNavigatedTo(navigationParameter, navigationMode, viewModelState);
			Position = Convert.ToByte(navigationParameter);
			if (!ApplicationData.Current.LocalSettings.Values.ContainsKey(DropboxHelperService.USER_TOKEN) && (bool)ApplicationData.Current.LocalSettings.Values[DropboxHelperService.ASK_ON_START])
			{
				await DropboxHelperService.ShowDropboxServiceBroker(_dropboxRepo);
			}
		}

		private void Importing(ItemClickEventArgs args, INavigationService navigationService, IGraphsRepo graphsRepo)
		{
			graphsRepo.Graphs[Position].FolderId = ((DropboxDataViewModel)args.ClickedItem).FolderId;
			graphsRepo.Graphs[Position].Data = ((DropboxDataViewModel)args.ClickedItem).Data;
			navigationService.GoBack();
		}

		public IList<object> SelectedItems
		{
			get
			{
				return selectedItems;
			}
			set
			{
				SetProperty(ref selectedItems, value);
			}
		}

		[RestorableState]
		public byte Position { get; set; }
	}
}