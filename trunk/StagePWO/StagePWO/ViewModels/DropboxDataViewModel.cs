﻿using StagePWO.Common;
using StagePWO.Model;
using System;
using System.Runtime.Serialization;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;

namespace StagePWO.ViewModels
{
	[DataContract]
	public class DropboxDataViewModel : BindableBaseContracted
	{
		private static readonly Uri uri = new Uri("ms-appx:/Assets/dropboxPlaceholder.png");

		private BitmapImage image;
		private string title;

		[DataMember]
		public string Hash { get; set; }

		[DataMember]
		public byte[] ImageData { get; set; }

		public async void InitImage()
		{
			if (ImageData == null || Image.UriSource != uri)
			{
				return;
			}
			try
			{
				using (IRandomAccessStream stream = new InMemoryRandomAccessStream())
				{
					await stream.WriteAsync(ImageData.AsBuffer());
					stream.Seek(0);

					BitmapImage image = new BitmapImage();
					await image.SetSourceAsync(stream);
					this.Image = image;
				}
			}
			catch (Exception) { }
		}

		public BitmapImage Image
		{
			get
			{
				if (image == null)
				{
					SetProperty(ref image, new BitmapImage(uri));
				}
				return image;
			}
			set { SetProperty(ref image, value); }
		}

		[DataMember]
		public DateTime Date { get; set; }

		[DataMember]
		public MeasurementData Data { get; set; }

		[DataMember]
		public string FolderId { get; set; }

		[DataMember]
		public string Title { get { return title; } set { SetProperty(ref title, value); } }
	}
}