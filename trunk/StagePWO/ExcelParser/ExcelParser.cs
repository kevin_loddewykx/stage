﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ExcelParserCore
{
	public class ExcelParser
	{
		private IZipArchive zipArchive;
		private List<string> sharedStrings;

		public ExcelParser(IZipArchive zipArchive)
		{
			this.zipArchive = zipArchive;
		}

		public List<string> SharedStrings
		{
			get
			{
				if (sharedStrings == null)
				{
					GetSharedStrings();
				} return sharedStrings;
			}
			set { sharedStrings = value; }
		}

		public Dictionary<int, string> GetSheets()
		{
			IZipArchiveEntry workbook = zipArchive.GetEntry("xl/workbook.xml");
			Dictionary<int, string> sheets = new Dictionary<int, string>();
			using (Stream sr = workbook.Open())
			{
				XDocument xdoc = XDocument.Load(sr);
				XNamespace ns = xdoc.Root.GetDefaultNamespace();
				XNamespace r = xdoc.Root.GetNamespaceOfPrefix("r");
				
				sheets = xdoc.Root.Descendants(ns + "sheet").Select(sheet => new
				{
					Id = Convert.ToInt32(Regex.Replace(sheet.Attribute(r + "id").Value, "[^0-9]", "")),
					Name = (string)sheet.Attribute("name").Value
				}).ToDictionary(sheet => sheet.Id, sheet => sheet.Name);
			}
			return sheets;
		}

		public void GetSharedStrings()
		{
			IZipArchiveEntry workbook = zipArchive.GetEntry("xl/sharedStrings.xml");

			SharedStrings = new List<string>();
			using (Stream sr = workbook.Open())
			{
				XDocument xdoc = XDocument.Load(sr);
				XNamespace ns = xdoc.Root.Attribute("xmlns").Value;
				sharedStrings = xdoc.Root.Descendants(ns + "t").Select(a => a.Value).ToList();
			}
		}

		public Dictionary<int, Dictionary<int, string>> GetDataSheet(int id)
		{
			if (sharedStrings == null)
			{
				GetSharedStrings();
			}
			Dictionary<int, Dictionary<int, string>> dataSheet = new Dictionary<int, Dictionary<int, string>>();
			IZipArchiveEntry workbook = zipArchive.GetEntry("xl/worksheets/sheet" + id + ".xml");
			using (Stream sr = workbook.Open())
			{
				XDocument xdoc = XDocument.Load(sr);
				XNamespace ns = xdoc.Root.Attribute("xmlns").Value;
				dataSheet = xdoc.Root.Descendants(ns + "row").Select(row => new
				{
					Row = Convert.ToInt32(row.Attribute("r").Value),
					Cells = row.Descendants(ns + "c").Select(cell => new
					{
						Column = GetColumnNumber(cell.Attribute("r").Value),
						Value = (cell.Attribute("t") != null && cell.Attribute("t").Value.Equals("s")) ? SharedStrings[Convert.ToInt32(cell.Element(ns + "v").Value)] : cell.Element(ns + "v").Value
					}).ToDictionary(cell => cell.Column, cell => cell.Value)
				}).ToDictionary(row => row.Row, row => row.Cells);
			}
			return dataSheet;
		}

		private int GetColumnNumber(string name)
		{
			int number = 0;
			int pow = 1;
			name = name.TrimEnd(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });
			for (int i = name.Length - 1; i >= 0; i--)
			{
				number += (name[i] - 'A' + 1) * pow;
				pow *= 26;
			}

			return number;
		}
	}
}