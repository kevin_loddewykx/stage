﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ExcelParserCore
{
	public interface IZipArchive
	{
		IZipArchiveEntry GetEntry(string path);
	}

	public interface IZipArchiveEntry
	{
		Stream Open();
	}


}
