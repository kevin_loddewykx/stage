﻿using Microsoft.Practices.Prism.StoreApps.Interfaces;
using System;

namespace UnitTestLibrary1.Mocks
{
	public class MockResourceLoader : IResourceLoader
	{
		public MockResourceLoader()
		{
			GetStringDelegate = s => s;
		}

		public Func<string, string> GetStringDelegate { get; set; }
		public string GetString(string resource)
		{
			return GetStringDelegate(resource);
		}
	}
}