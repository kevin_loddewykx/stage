﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using StagePWO.Model;
using Windows.Storage;
using System.Threading.Tasks;
using AsyncAssert = Microsoft.VisualStudio.TestPlatform.UnitTestFramework.AppContainer.Assert;
using StagePWO.ViewModels;
using StagePWO.Services;
using UnitTestLibrary1.Mocks;
using Windows.Storage.AccessCache;
using StagePWO.Repository;
using StagePWO.Exceptions;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace UnitTestLibrary1
{
	[TestClass]
	public class UnitTest1
	{

		[TestMethod]
		public async Task TestDimensionChecks()
		{
			StorageFile testFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile.csv"));
			StorageFile testFile_ColumnError = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_ColumnError.csv"));
			StorageFile testFile_RowError = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_RowError.csv"));
			NewGraphPageViewModel dataViewModel = new NewGraphPageViewModel(null, null, null, null, null);
			NewGraphPageViewModel dataViewModel2 = new NewGraphPageViewModel(null, null, null, null, null);
			NewGraphPageViewModel dataViewModel3 = new NewGraphPageViewModel(null, null, null, null, null);

			StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.SpeedToken, testFile);
			StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.TorqueToken, testFile_ColumnError);
			StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.EfficiencyToken, testFile);

			await AsyncAssert.ThrowsException<ImportException>(async () => await new CSVImporterService(new MockResourceLoader()).Import(true, null));

			StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.TorqueToken, testFile_RowError);
			await AsyncAssert.ThrowsException<ImportException>(async () => await new CSVImporterService(new MockResourceLoader()).Import(true, null));

			StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.TorqueToken, testFile);
			Assert.IsNotNull(await new CSVImporterService(new MockResourceLoader()).Import(true, null));


		}

		[TestMethod]
		public async Task TestLoopOrder()
		{
			TaskCompletionSource<object> taskSource = new TaskCompletionSource<object>();
			await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
				{
					try
					{
						StorageFile testFile_Speed = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_Speed.csv")).AsTask();
						StorageFile testFile_Torque = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_Torque.csv")).AsTask();
						StorageFile testFile_Efficiency = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_Efficiency.csv")).AsTask();
						GraphsRepo repo = new GraphsRepo(new MockSessionStateService());
						repo.Graphs.Add(new DataViewModel(0, null));

						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.SpeedToken, testFile_Speed);
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.TorqueToken, testFile_Torque);
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.EfficiencyToken, testFile_Efficiency);

						repo.Graphs[0].Data = await new CSVImporterService(new MockResourceLoader()).Import(true, null);
						MeasurementData data = repo.Graphs[0].Data;
						Assert.AreEqual(6, data.TripletColumns);
						Assert.AreEqual(5, data.TripletRows);
						await repo.Graphs[0].Import();
						Assert.AreEqual(6 * 2, data.XValues.Length);
						Assert.AreEqual(5 * 2, data.YValues.Length);
						Assert.AreEqual(6 * 5 * 4, data.Values.Length);
						Assert.AreEqual(5 * 2, data.Values.GetUpperBound(0) + 1); // Amount of rows
						Assert.AreEqual(6 * 2, data.Values.GetUpperBound(1) + 1); // Amount of columns
						taskSource.SetResult(null);
					}
					catch (Exception e)
					{
						taskSource.SetException(e);
					}
				});
			await taskSource.Task;
		}
	}
}