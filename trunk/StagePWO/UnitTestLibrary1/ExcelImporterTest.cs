﻿using ExcelParserWrapper;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using ExcelParserCore;
using System.Globalization;
using UnitTestLibrary1.Mocks;
using StagePWO.ViewModels;
using StagePWO.Model;
using StagePWO.Repository;
using StagePWO.Services;
using Windows.Storage.AccessCache;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace UnitTestLibrary1
{
	[TestClass]
	public class ExcelImporterTest
	{

		[TestMethod]
		public async Task TestExcelSheets()
		{
			StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/test.xlsx")).AsTask();
			IRandomAccessStream stream = await file.OpenReadAsync();
			IZipArchive zipArchive = new ZipArchiveWrapper(stream.AsStream());
			ExcelParser parser = new ExcelParser(zipArchive);
			Dictionary<int, string> data = parser.GetSheets();
			Assert.AreEqual(3, data.Count);
			Assert.IsTrue(data.ContainsKey(1));
			Assert.IsTrue(data.ContainsKey(2));
			Assert.IsTrue(data.ContainsKey(3));
			Assert.AreEqual("qsdqs", data[1]);
			Assert.AreEqual("Blad2", data[2]);
			Assert.AreEqual("Blad3", data[3]);

			Assert.AreEqual("sdfd", parser.GetDataSheet(1)[1][1]);
			Assert.AreEqual("sdf", parser.GetDataSheet(1)[1][2]);
			Assert.AreEqual("45", parser.GetDataSheet(1)[6][4]);
		}

		[TestMethod]
		public async Task TestExcelSheets2()
		{
			StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Meting_KoppelRelatief.xlsx")).AsTask();
			IRandomAccessStream stream = await file.OpenReadAsync();
			IZipArchive zipArchive = new ZipArchiveWrapper(stream.AsStream());
			ExcelParser parser = new ExcelParser(zipArchive);
			Dictionary<int, Dictionary<int, string>> sheet = parser.GetDataSheet(1);
			Assert.AreEqual(40, sheet.Count);
			Assert.AreEqual(40, sheet[1].Count);
			Assert.AreEqual(1, sheet.First().Key);
			Assert.AreEqual("0.13", sheet[40][1]);
			Assert.AreEqual("0.1", sheet[40][40]);
			double s;

			if (!(double.TryParse(sheet[40][1], out s)))
			{
				s = double.Parse(sheet[40][1], CultureInfo.InvariantCulture);
			}
			Assert.AreEqual(0.13, s);
			if (!(double.TryParse(sheet[1][1], out s)))
			{
				s = double.Parse(sheet[1][1], CultureInfo.InvariantCulture);
			}
			Assert.AreEqual(0, s);
			if (!(double.TryParse(sheet[20][26], out s)))
			{
				s = double.Parse(sheet[20][26], CultureInfo.InvariantCulture);
			}
			Assert.AreEqual(0.71, s);

		}

		[TestMethod]
		public async Task TestExcelSheets3()
		{
			TaskCompletionSource<object> taskSource = new TaskCompletionSource<object>();
			await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
				{
					try
					{
						StorageFile testFile_Speed = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Meting_ToerentalRelatief.xlsx")).AsTask();
						StorageFile testFile_Torque = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Meting_KoppelRelatief.xlsx")).AsTask();
						StorageFile testFile_Efficiency = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Meting_Efficientie.xlsx")).AsTask();
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.SpeedToken, testFile_Speed);
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.TorqueToken, testFile_Torque);
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.EfficiencyToken, testFile_Efficiency);

						GraphsRepo repo = new GraphsRepo(new MockSessionStateService());
						repo.Graphs.Add(new DataViewModel(0, null));
						repo.Graphs[0].Data = await new ExcelImporterService(new MockResourceLoader()).Import(true, null);
						MeasurementData data = repo.Graphs[0].Data;
						Assert.AreEqual(40, data.TripletColumns);
						Assert.AreEqual(40, data.TripletRows);
						await repo.Graphs[0].Import();
						Assert.AreEqual(40 * 2, data.XValues.Length);
						Assert.AreEqual(40 * 2, data.YValues.Length);
						Assert.AreEqual(40 * 40 * 4, data.Values.Length);
						Assert.AreEqual(40 * 2, data.Values.GetUpperBound(0) + 1); // Amount of rows
						Assert.AreEqual(40 * 2, data.Values.GetUpperBound(1) + 1); // Amount of columns

						Assert.AreEqual(1600, data.TripletList.Count);
						Assert.AreEqual(0, data.TripletList[0].Torque);
						Assert.AreEqual(0.1, data.TripletList[1600 - 1].Torque);
						Assert.AreEqual(0.71, data.TripletList[19 * 40 + 26 - 1].Torque);
						taskSource.SetResult(null);
					}
					catch (Exception e)
					{
						taskSource.SetException(e);
					}
				});
			await taskSource.Task;
		}
	}
}