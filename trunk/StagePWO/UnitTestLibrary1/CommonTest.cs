﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using StagePWO.Model;
using StagePWO.Repository;
using StagePWO.Services;
using StagePWO.ViewModels;
using System;
using System.Threading.Tasks;
using UnitTestLibrary1.Mocks;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.UI.Core;

namespace UnitTestLibrary1
{
	[TestClass]
	public class CommonTest
	{
		[TestMethod]
		public void TestSettings()
		{
			Settings settings = new Settings();
			settings.IntervalLevel = 5;
			settings.IntervalLevel = 15;
			settings.IntervalLevel = 20;
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => { settings.IntervalLevel = 0; });
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => { settings.IntervalLevel = -5; });
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => { settings.IntervalLevel = 16; });
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => { settings.IntervalLevel = 21; });
		}

		[TestMethod]
		public void TestValuesRestore()
		{
			MeasurementData data = new MeasurementData();
			data.TripletColumns = 6;
			data.TripletRows = 5;
			double[,] values = new Double[5 * 2, 6 * 2];
			for (int i = 0; i < 5 * 2; ++i)
			{
				for (int j = 0; j < 6 * 2; ++j)
				{
					values[i, j] = i + j;
				}
			}
			data.Values = values;
			double[] valuesFlat = data.ValuesFlat;
			data.Values = null;
			data.ValuesFlat = valuesFlat;
			CollectionAssert.AreEquivalent(values, data.Values);
			Assert.AreEqual(values.GetUpperBound(0), data.Values.GetUpperBound(0));
			Assert.AreEqual(values.GetUpperBound(1), data.Values.GetUpperBound(1));
		}

		[TestMethod]
		public async Task TestValidCheck()
		{
			TaskCompletionSource<object> taskSource = new TaskCompletionSource<object>();
			await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
				{
					try
					{
						StorageFile testFile_Speed = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_Speed.csv")).AsTask();
						StorageFile testFile_Torque = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_Torque.csv")).AsTask();
						StorageFile testFile_Efficiency = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/testFile_Efficiency.csv")).AsTask();
						GraphsRepo repo = new GraphsRepo(new MockSessionStateService());
						repo.Graphs.Add(new DataViewModel(0, null));

						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.SpeedToken, testFile_Speed);
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.TorqueToken, testFile_Torque);
						StorageApplicationPermissions.FutureAccessList.AddOrReplace(NewGraphPageViewModel.EfficiencyToken, testFile_Efficiency);

						repo.Graphs[0].Data = await new CSVImporterService(new MockResourceLoader()).Import(true, null);
						MeasurementData data = repo.Graphs[0].Data;

						Assert.IsTrue(data.IsValidPoint(0.39, 0.22));
						Assert.IsTrue(data.IsValidPoint(0.39, 0.10));
						Assert.IsTrue(data.IsValidPoint(0.15, 0.22));
						Assert.IsTrue(data.IsValidPoint(0.15, 0.10));

						/*
						 *			  0.15, 0.23			  0.39, 0.23
						 * 0.14, 0.22 +-----------------------+ 0.40, 0.22
						 *			  |                       |
						 *			  |                       |
						 *			  |                       |
						 * 0.14, 0.10 +-----------------------+ 0.40, 0.10
						 *            0.15, 0.09              0.39, 0.09
						 */
						Assert.IsFalse(data.IsValidPoint(0.40, 0.22));
						Assert.IsFalse(data.IsValidPoint(0.39, 0.23));
						Assert.IsFalse(data.IsValidPoint(0.40, 0.10));
						Assert.IsFalse(data.IsValidPoint(0.39, 0.09));
						Assert.IsFalse(data.IsValidPoint(0.14, 0.22));
						Assert.IsFalse(data.IsValidPoint(0.15, 0.23));
						Assert.IsFalse(data.IsValidPoint(0.14, 0.10));
						Assert.IsFalse(data.IsValidPoint(0.15, 0.09));

						await repo.Graphs[0].Import();
						// Calculated	Speed = 0.16666667, Torque = 0.15
						// Nearest		Speed = 0.15, Torque = 0.16
						Assert.AreEqual(31.5, data.Values[1, 1], 2.5);

						taskSource.SetResult(null);
					}
					catch (Exception e)
					{
						taskSource.SetException(e);
					}
				});
			await taskSource.Task;
		}
	}
}