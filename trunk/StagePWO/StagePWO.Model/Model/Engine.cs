﻿using System.Collections.Generic;

namespace StagePWO.Model
{
	public class Engine
	{
		public enum ErrorTags
		{
			Speed,
			Torque,
			EE
		}
		// Windows phone 8 doesn't support data annotations
		// so I cannot use this framework
		public List<ErrorTags> ValidateProperties()
		{
			List<ErrorTags> errors = new List<ErrorTags>();
			if (NominalSpeed <= 0 || double.IsInfinity(NominalSpeed))
			{
				errors.Add(ErrorTags.Speed);
			}
			if (NominalTorque <= 0 || double.IsInfinity(NominalTorque))
			{
				errors.Add(ErrorTags.Torque);
			}
			if (string.IsNullOrEmpty(EELabel))
			{
				errors.Add(ErrorTags.EE);
			}
			return errors;
		}

		// Nominaal toerental (tr/min)
		public int NominalSpeed { get; set; }


		// Nominaal koppel (Nm)
		public double NominalTorque { get; set; }

		// IE label
		public string EELabel { get; set; }

		public string ExtraInfo { get; set; }
	}
}