﻿using System;

namespace StagePWO.Model
{
	public class Settings : ISettings
	{
		private int internalLevel = 5;
		public bool ContourOn { get; set; }

		public bool ScatterOn { get; set; }

		public int IntervalLevel
		{
			get { return internalLevel; }
			set
			{
				if (value < 1 || value > 20 || value % 5 != 0)
				{
					throw new ArgumentOutOfRangeException("The interval level must be a multiple of 5 and being located between 5 and 20.");
				}
				internalLevel = value;
			}
		}
	}
}