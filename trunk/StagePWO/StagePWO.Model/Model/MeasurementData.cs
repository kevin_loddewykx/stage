﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace StagePWO.Model
{
	[DataContract]
	public class MeasurementData
	{

		// the same measurements, but now organized into a matrix
		// this matrix has the same structure as the input
		private EETriplet[,] eetriplets;

		private double[,] rbfdata;

		public MeasurementData()
		{
			TripletList = new List<EETriplet>();
		}

		[DataMember]
		public Engine Engine { get; set; }

		public double[,] Values { get; set; }

		[DataMember(Order = 1)]
		public double[] ValuesFlat
		{
			get
			{
				if (Values != null)
				{
					return Values.Cast<double>().ToArray(); ;
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (value != null)
				{
					Values = new double[TripletRows * 2, TripletColumns * 2];
					for (int i = 0; i < TripletRows * 2; i++)
					{
						for (int j = 0; j < TripletColumns * 2; j++)
						{
							Values[i, j] = value[j + i * TripletColumns * 2];
						}
					}
				}
			}
		}

		[DataMember]
		public double[] XValues { get; set; }

		[DataMember]
		public double[] YValues { get; set; }

		[DataMember]
		public int TripletColumns { get; set; }

		[DataMember]
		public int TripletRows { get; set; }

		[DataMember]
		public List<EETriplet> TripletList { get; private set; }

		public void AddTriplet(EETriplet triplet)
		{
			TripletList.Add(triplet);
		}

		/// <summary>
		/// returns de list of measurements as an array of doubles.
		/// Numbers of rows equals number of measurements
		/// Column 0: speed
		/// Column 1: torque
		/// Column 2: efficiency
		/// </summary>
		public double[,] RBFData
		{
			get
			{
				if (rbfdata == null)
				{
					int rows = TripletList.Count;
					int cols = 3;
					rbfdata = new double[rows, cols];
					for (int i = 0; i < rows; i++)
					{
						rbfdata[i, 0] = TripletList[i].Speed;
						rbfdata[i, 1] = TripletList[i].Torque;
						rbfdata[i, 2] = double.IsNaN(TripletList[i].Efficiency) ? 0.0 : TripletList[i].Efficiency;
					}
				}
				return rbfdata;
			}
		}

		/// <summary>
		/// Return reference data as a matrix
		/// rows -> torque
		/// cols -> speed
		/// </summary>
		private EETriplet[,] EETriplets
		{
			get
			{
				if (eetriplets == null)
				{
					eetriplets = new EETriplet[TripletRows, TripletColumns];
					List<EETriplet>.Enumerator enumer = TripletList.GetEnumerator();
					for (int i = 0; i < TripletRows; i++)
					{
						for (int j = 0; j < TripletColumns; j++)
						{
							enumer.MoveNext();
							eetriplets[i, j] = enumer.Current;
						}
					}
				}
				return eetriplets;
			}
		}

		public bool IsValidPoint(double speed, double torque)
		{
			for (int i = 1; i < TripletRows; i++)
			{
				for (int j = 1; j < TripletColumns; j++)
				{
					if (EETriplets[i, j - 1].Speed <= speed && speed <= EETriplets[i, j].Speed &&
						((EETriplets[i - 1, j].Torque <= torque && torque <= EETriplets[i, j].Torque) || (EETriplets[i - 1, j].Torque >= torque && torque >= EETriplets[i, j].Torque)) &&
						EETriplets[i, j - 1].Efficiency != 0 && EETriplets[i - 1, j].Efficiency != 0 &&
						EETriplets[i, j].Efficiency != 0 && EETriplets[i - 1, j - 1].Efficiency != 0)
					{
						return true;
					}
				}
			}

			return false;
		}

		public IEnumerable<EETriplet> GetScatterPoints()
		{
			return TripletList.Where((item) => ((EETriplet)item).Efficiency != 0);
		}

		public Task Initialize(CancellationToken token)
		{
			return Task.Factory.StartNew(() =>
			{
				alglib.rbfmodel model;
				alglib.rbfreport rep;
				alglib.rbfcreate(2, 1, out model);
				alglib.rbfsetpoints(model, RBFData);
				alglib.rbfsetalgomultilayer(model, 0.40, 6, 1.0e-3);
				alglib.rbfbuildmodel(model, out rep);

				token.ThrowIfCancellationRequested();

				if (1 == rep.terminationtype)
				{
					double[,] tempValues = new double[TripletRows * 2, TripletColumns * 2];
					XValues = new double[TripletColumns * 2];
					YValues = new double[TripletRows * 2];
					for (int i = 0; i < TripletRows * 2; ++i)
					{
						token.ThrowIfCancellationRequested();

						for (int j = 0; j < TripletColumns * 2; ++j)
						{
							if (IsValidPoint(2.0 / (TripletColumns * 2.0) * i, 1.5 / (TripletRows * 2.0) * j))
							{
								tempValues[i, j] = alglib.rbfcalc2(model, 2.0 / (TripletColumns * 2.0) * i, 1.5 / (TripletRows * 2.0) * j);
							}
							else
							{
								tempValues[i, j] = double.NaN;
							}

							if (i == 0)
							{
								XValues[j] = 2.0 / (TripletColumns * 2) * j;
							}
						}
						YValues[i] = 1.5 / (TripletRows * 2) * i;
					}
					Values = tempValues;
				}
				else
				{
					throw new Exception("Could not create function to plot");
				}
			}, token);
		}
	}
}