﻿namespace StagePWO.Model
{
	public interface ISettings
	{
		bool ContourOn { get; set; }

		bool ScatterOn { get; set; }

		int IntervalLevel { get; set; }
	}
}