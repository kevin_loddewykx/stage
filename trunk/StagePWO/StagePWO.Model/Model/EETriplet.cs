﻿using System;
using System.Runtime.Serialization;

namespace StagePWO.Model
{
	[DataContract]
	public class EETriplet
	{
		// relatief toerental
		private double speed;
		// relatief koppel
		private double torque;
		// relatief rendement
		private double efficiency;

		public EETriplet(double s, double t, double e)
		{
			this.Efficiency = e;
			if (e != 0)
			{
				this.Speed = s;
				this.Torque = t;
			}
			
		}

		[DataMember]
		public double Speed
		{
			get { return speed; }
			set
			{
				if (value < 0.0) throw new FormatException("Relative speed (toerental) cannot be negative");
				speed = value;
			}
		}

		[DataMember]
		public double Torque
		{
			get { return torque; }
			set
			{
				if (value < 0.0) throw new FormatException("Relative torque (koppel) cannot be negative");
				torque = value;
			}
		}

		[DataMember]
		public double Efficiency
		{
			get { return efficiency; }
			set
			{
				if (value < 0.0) throw new FormatException("Efficiency (rendement) cannot be negative");
				efficiency = value;
			}
		}

		public override string ToString()
		{
			return String.Format("[{0:0.000} of nominal speed (t/min), {1:0.000} of nominal torque (Nm), {2:0.00}%]", Speed, Torque, Efficiency);
		}
        
	}
}