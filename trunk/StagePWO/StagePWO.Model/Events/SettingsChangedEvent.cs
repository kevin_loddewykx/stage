﻿using Microsoft.Practices.Prism.PubSubEvents;
using StagePWO.Model;

namespace StagePWO.Events
{
	public class SettingsChangedEvent : PubSubEvent<Settings>
	{

	}
}