﻿using DropNetRT;
using DropNetRT.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Security.Authentication.Web;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace DropBoxUSageTest
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainPage : Page
	{
		public MainPage()
		{
			this.InitializeComponent();
			this.Loaded += Init;
		}

		private async void Init(object sender, RoutedEventArgs e)
		{
			DropNetClient client = null;
			if (!ApplicationData.Current.LocalSettings.Values.ContainsKey("USER_TOKEN") || !ApplicationData.Current.LocalSettings.Values.ContainsKey("USER_SECRET"))
			{
				client = new DropNetClient("oh5esyyz2vscejv", "w25p28gzfn9qq0d");
				UserLogin requestToken = await client.GetRequestToken();
				Uri url = new Uri(client.BuildAuthorizeUrl(requestToken));
				WebAuthenticationResult broker = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None, url);
				UserLogin accessToken = await client.GetAccessToken();
				ApplicationData.Current.LocalSettings.Values["USER_TOKEN"] = accessToken.Token;
				ApplicationData.Current.LocalSettings.Values["USER_SECRET"] = accessToken.Secret;
			}
			else
			{
				client = new DropNetClient("oh5esyyz2vscejv", "w25p28gzfn9qq0d", (string)ApplicationData.Current.LocalSettings.Values["USER_TOKEN"], (string)ApplicationData.Current.LocalSettings.Values["USER_SECRET"]);
			}
			client.UseSandbox = true;
			using (StreamWriter stream = new StreamWriter(new MemoryStream()))
			{
				await stream.WriteLineAsync("test");
				await stream.FlushAsync();
				stream.BaseStream.Position = 0;
				await client.Upload("", "test.txt", stream.BaseStream);
			}
			
				//await stream.BaseStream.FlushAsync();
				
			
			//await client.CreateFolder("test12345");

		}

		public void Example()
		{
			int[] numbers = { 5, 10, 8, 3, 6, 12 };

			//Query syntax:
			IEnumerable<int> numQuery1 =
				from num in numbers
				where num % 2 == 0
				orderby num
				select num;

			//Method syntax:
			IEnumerable<int> numQuery2 = numbers.Where(num => num % 2 == 0).OrderBy(n => n);
		}
	}
}
