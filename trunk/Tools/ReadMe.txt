Tools
=====

Lynx toolkit
  A collection of small command line developer tools.
  http://lynx.codeplex.com/

NuGet
  NuGet command line bootstrapper
  http://nuget.codeplex.com/

Pepita
  Utilities for creating and retrieving nuget packages as part of a build
  http://code.google.com/p/pepita/
  
HtmlHelp
  Compiler for HTML help files (.hhc)
  http://msdn.microsoft.com/en-us/library/windows/desktop/ms670169(v=vs.85).aspx
  http://msdn.microsoft.com/en-us/library/windows/desktop/ms669985(v=vs.85).aspx
  http://en.wikipedia.org/wiki/Microsoft_Compiled_HTML_Help 